use lazy_static::lazy_static;
use proc_macro::{Span, TokenStream};
use proc_macro_error::abort;
use quote::ToTokens;
use regex::Regex;
use syn::{Item, ItemStruct};

lazy_static! {
    pub static ref VEC_REGEX: Regex = Regex::new("(?:std::(?:vec::)?)?Vec<(.*)>").unwrap();
    pub static ref OPTION_REGEX: Regex = Regex::new("(?:std::(?:option::)?)?Option<(.*)>").unwrap();
}

#[derive(Debug)]
pub enum Wrapper {
    Vec,
    Option,
}

pub fn unwrap_type(ty: String) -> (String, Option<Wrapper>) {
    if let Some(captures) = VEC_REGEX.captures(&*ty) {
        (captures.get(1).unwrap().as_str().to_string(), Some(Wrapper::Vec))
    } else if let Some(captures) = OPTION_REGEX.captures(&*ty) {
        (captures.get(1).unwrap().as_str().to_string(), Some(Wrapper::Option))
    } else {
        (ty, None)
    }
}

struct FieldMetaData {
    name: String,
    ty: String,
    wrapper: Option<Wrapper>,
}

pub fn try_derive_builder(item: TokenStream) -> Result<TokenStream, ()> {
    // Parse the item token stream into a struct
    let item: ItemStruct = if let Item::Struct(s) = syn::parse::<Item>(item).map_err(|_| abort!(Span::call_site(), "Could not parse"))? {
        s
    } else {
        abort!(Span::call_site(), "Can only be used on struct")
    };

    // Get the FieldMetaData for all fields
    let fields: Vec<FieldMetaData> = item
        .fields
        .into_iter()
        .map(|f| {
            let name = f.ident.unwrap().to_string();
            let (ty, wrapper) = unwrap_type(f.ty.to_token_stream().to_string().replace(' ', ""));
            FieldMetaData { name, ty, wrapper }
        })
        .collect();

    // Generate the constructor of the builder
    let constructor = format!(
        r#"pub fn new({}) -> Self {{
            Self{{
                value: {name} {{
                    {}
                }}
            }}
        }}"#,
        fields.iter().filter(|f| f.wrapper.is_none()).map(|f| format!("{}: {}", f.name, f.ty)).collect::<Vec<String>>().join(", "),
        fields
            .iter()
            .map(|f| match f.wrapper.as_ref() {
                Some(w) => format!(
                    "{}: {}",
                    f.name,
                    match w {
                        Wrapper::Option => "std::option::Option::None".to_string(),
                        Wrapper::Vec => "std::vec::Vec::new()".to_string(),
                    }
                ),
                None => f.name.clone(),
            })
            .collect::<Vec<String>>()
            .join(", "),
        name = item.ident,
    );

    // Generate all set methods
    let methods = fields
        .iter()
        .filter(|f| f.wrapper.is_some())
        .map(|f| {
            format!(
                r#"
                /// Set the field {name} to the given value
                pub fn {name}(mut self, val: {ty}) -> Self{{
                    if self.value.{name}.{cond} {{
                        panic!("Field {name} cannot be reset");
                    }}

                    self.value.{name} = {value};
                    self
                }}"#,
                name = f.name,
                ty = match f.wrapper.as_ref().unwrap() {
                    Wrapper::Vec => format!("std::vec::Vec<{}>", f.ty),
                    Wrapper::Option => f.ty.clone(),
                },
                cond = match f.wrapper.as_ref().unwrap() {
                    Wrapper::Vec => "len() > 0",
                    Wrapper::Option => "is_some()",
                },
                value = match f.wrapper.as_ref().unwrap() {
                    Wrapper::Vec => "val",
                    Wrapper::Option => "Some(val)",
                }
            )
        })
        .collect::<Vec<String>>()
        .join("\n");

    // Construct the final implementation
    Ok(format!(
        r#"
        /// Builder for the {name} structure
        pub struct {name}Builder {{
                value: {name},
            }}

            impl {name}Builder {{
                /// Instantiate the builder with the fields {}
                #[allow(clippy::too_many_arguments)]
                {constructor}

                {methods}
            }}

            impl crate::builder::Builder for {name}Builder{{
                type Value = {name};

                fn build(self) -> <{name}Builder as crate::builder::Builder>::Value{{ self.value }}
            }}"#,
        fields.iter().map(|f| &f.name).cloned().collect::<Vec<String>>().join(", "),
        name = item.ident
    )
    .parse()
    .unwrap())
}
