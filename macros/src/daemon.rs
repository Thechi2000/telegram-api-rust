use crate::constants::*;
use proc_macro::Span;
use proc_macro::TokenStream;
use proc_macro_error::abort;
use quote::ToTokens;
use syn::parse::Parser;
use syn::punctuated::Punctuated;
use syn::{Expr, ExprLit, FnArg, Item, Lit, PatType, Token, Type};

pub fn try_daemon(attr: TokenStream, item: TokenStream) -> Result<TokenStream, ()> {
    // Parse the given token stream into a function item
    let item = if let Item::Fn(f) = syn::parse::<Item>(item).map_err(|_| abort!(Span::call_site(), "Could not parse"))? {
        f
    } else {
        abort!(Span::call_site(), "Can only be applied to functions")
    };

    // Parse the attr token stream into a list of expressions
    let attr = Punctuated::<Expr, Token![,]>::parse_terminated.parse(attr).map_err(|_| abort!(Span::call_site(), "Could not parse attributes"))?;

    let interval = attr
        .iter()
        .find_map(|p| match p {
            Expr::Assign(a) => match a.left.as_ref() {
                Expr::Path(p) => p.path.segments.len() == 1 && p.path.segments.first().unwrap().ident == "interval",
                _ => false,
            }
            .then(|| a.right.as_ref()),
            _ => None,
        })
        .map(|e| if let Expr::Lit(ExprLit { lit: Lit::Int(i), .. }) = e {
            i.base10_parse::<usize>().map_err(|_| abort!(i, "Could not parse into usize")).unwrap()
        } else {
            abort!(e, "Should be a integer literal")
        });

    let type_bounds = item
        .sig
        .inputs
        .iter()
        .map(|a| {
            format!(
                "for<'a> {}: {FROM_DAEMON}<'a, T>",
                match a {
                    FnArg::Receiver(_) => abort!(a, "Daemons are not allowed self parameters"),
                    FnArg::Typed(t) => {
                        match t.ty.as_ref() {
                            Type::Reference(r) => {
                                format!("&'a {} {}", r.mutability.map(|_| "mut").unwrap_or(""), r.elem.to_token_stream())
                            }
                            _ => t.ty.to_token_stream().to_string(),
                        }
                    }
                }
            )
        })
        .collect::<Vec<_>>()
        .join(", ");

    let daemon_args = item.sig
        .inputs
        .iter()
        .map(|a| {
            if let FnArg::Typed(PatType { ty, .. }) = a {
                format!("<{} as {FROM_DAEMON}<T>>::from_daemon(bot, state)", ty.to_token_stream())
            } else {
                abort!(a, "Function should not take self parameter")
            }
        })
        .collect::<Vec<_>>()
        .join(", ");

    let launch_body = if let Some(interval) = interval {
        format!(r#"
            let mut interval = {INTERVAL}({DURATION}::from_secs({interval}));
            loop {{
                interval.tick().await;

                let l = lock.lock().await;

                let bot = bot.as_ref();
                let state = state.as_ref().as_ref();

                {name}({daemon_args}).await;
            }}
        "#,
                name = item.sig.ident
        )
    } else {
        format!(r#"
            let bot = bot.as_ref();
            let state = state.as_ref().as_ref();

            {name}({daemon_args}).await;
        "#,
                name = item.sig.ident
        )
    };

    Ok(format!(
        r#"
        {vis} fn {name}_daemon<T: {SYNC} + {SEND} + 'static>() -> {DAEMON_STRUCT}<T>
        where {type_bounds} {{
            struct D{{}};

            #[async_trait::async_trait]
            impl<T: {SYNC} + {SEND} + 'static> {DAEMON_TRAIT}<T> for D
            where {type_bounds} {{
                async fn launch(&self, bot: {ARC}<{BOT}>, state: {ARC}<{OPTION}<{BOT_STATE}<T>>>, lock: {ARC}<{MUTEX}<()>>){{
                    {launch_body}
                }}
            }}

            {DAEMON_STRUCT}{{
                daemon: Box::new(D{{}}),
                syntax: "{name}",
            }}
        }}

        {}
        "#,
        item.to_token_stream(),
        vis = item.vis.to_token_stream(),
        name = item.sig.ident
    )
    .parse()
    .unwrap())
}

pub fn try_daemons(item: TokenStream) -> Result<TokenStream, ()> {
    let daemons = Punctuated::<Expr, Token![,]>::parse_terminated.parse(item).map_err(|_| abort!(Span::call_site(), "Could not parse list"))?;
    let vec = daemons
        .into_iter()
        .map(|e| if let Expr::Path(p) = e { format!("{}_daemon()", p.to_token_stream()) } else { abort!(e, "Should be a function name") })
        .collect::<Vec<String>>()
        .join(", ");

    Ok(format!("vec![{vec}]").parse().unwrap())
}
