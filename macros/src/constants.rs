pub const HANDLER_ERROR: &str = "telegram_bot2::__private::HandlerError";
pub const UPDATE: &str = "telegram_bot2::models::Update";
pub const BOT_STATE: &str = "telegram_bot2::BotState";
pub const BOT: &str = "telegram_bot2::Bot";

pub const COMMAND_HANDLER: &str = "telegram_bot2::__private::command::CommandHandler";
pub const GENERIC_HANDLER: &str = "telegram_bot2::__private::GenericHandler";

pub const COMMAND_HANDLER_TRAIT: &str = "telegram_bot2::__private::command::Handler";
pub const HANDLER_TRAIT: &str = "telegram_bot2::__private::Handler";
pub const DAEMON_STRUCT: &str = "telegram_bot2::__private::daemon::DaemonStruct";
pub const DAEMON_TRAIT: &str = "telegram_bot2::__private::daemon::Daemon";

pub const FROM_UPDATE: &str = "telegram_bot2::FromUpdate";
pub const FROM_DAEMON: &str = "telegram_bot2::FromDaemon";

pub const GROUP_COMMANDS: &str = "telegram_bot2::__private::command::group_commands";

pub const ARC: &str = "std::sync::Arc";
pub const MUTEX: &str = "tokio::sync::Mutex";

pub const DURATION: &str = "std::time::Duration";
pub const INTERVAL: &str = "tokio::time::interval";

pub const OPTION: &str = "std::option::Option";

pub const FROM_STR: &str = "std::str::FromStr";

pub const SYNC: &str = "std::marker::Sync";
pub const SEND: &str = "std::marker::Send";

pub const LOG: &str = "telegram_bot2::log";

pub const SENT_FILE_INFO: &str = "crate::models::SentFileInfo";
pub const SENT_FILE: &str = "crate::models::SentFile";
pub const FILE_HOLDER: &str = "crate::models::FileHolder";
