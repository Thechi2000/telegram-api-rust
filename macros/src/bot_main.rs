use proc_macro::{Span, TokenStream};
use proc_macro_error::abort;
use quote::ToTokens;
use syn::Item;

pub fn try_bot_main(_attr: TokenStream, item: TokenStream) -> Result<TokenStream, ()> {
    let item = if let Item::Fn(f) = syn::parse::<Item>(item).map_err(|_| abort!(Span::call_site(), "Could not parse"))? {
        f
    } else {
        abort!(Span::call_site(), "Can only be applied to functions")
    };

    Ok(format!(
        r#"
        #[tokio::main]
        async fn main(){{
            {}.launch().await;
        }}
        "#,
        item.block.to_token_stream()
    )
    .parse()
    .unwrap())
}
