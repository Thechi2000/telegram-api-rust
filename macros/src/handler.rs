use crate::constants::*;
use proc_macro::Span;
use proc_macro::TokenStream;
use proc_macro_error::abort;
use quote::ToTokens;
use syn::parse::Parser;
use syn::punctuated::Punctuated;
use syn::{Expr, FnArg, Item, Lit, Meta, NestedMeta, Token, Type};

pub fn try_handler(attr: TokenStream, item: TokenStream) -> Result<TokenStream, ()> {
    // Parse the given token stream into a function item
    let item = if let Item::Fn(f) = syn::parse::<Item>(item).map_err(|_| abort!(Span::call_site(), "Could not parse"))? {
        f
    } else {
        abort!(Span::call_site(), "Can only be applied to functions")
    };

    // Parse the attr token stream into a list of expressions
    let attr = Punctuated::<Meta, Token![,]>::parse_terminated.parse(attr).map_err(|_| abort!(Span::call_site(), "Could not parse attributes"))?;

    // Get the rank parameter (or 0 as default)
    let rank = attr
        .iter()
        .find(|m| m.path().segments.len() == 1 && m.path().segments.first().unwrap().ident == "rank")
        .map(|m| {
            if let Meta::NameValue(v) = m {
                if let Lit::Int(v) = &v.lit { v.base10_parse::<usize>().ok() } else { None }.unwrap_or_else(|| abort!(v, "'rank' should be a usize"))
            } else {
                abort!(m, "'rank' should have a value");
            }
        })
        .unwrap_or(0);

    let updates = attr
        .iter()
        .find(|m| m.path().segments.len() == 1 && m.path().segments.first().unwrap().ident == "restrict")
        .map(|m| {
            if let Meta::List(list) = m {
                list.nested
                    .iter()
                    .map(|m| {
                        if let NestedMeta::Meta(Meta::Path(p)) = m {
                            p.to_token_stream().to_string()
                        } else {
                            abort!(m, "'restrict' should contains UpdateType variants")
                        }
                    })
                    .collect::<Vec<_>>()
            } else {
                abort!(m, "'restrict' should be a list")
            }
        })
        .unwrap_or(vec![])
        .join(", ");

    // Get the function visibility
    let vis = item.vis.to_token_stream().to_string();

    let type_bounds = item
        .sig
        .inputs
        .iter()
        .map(|a| {
            format!(
                "for<'a> {}: {FROM_UPDATE}<'a, T>",
                match a {
                    FnArg::Receiver(_) => abort!(a, "Handlers are not allowed self parameters"),
                    FnArg::Typed(t) => {
                        match t.ty.as_ref() {
                            Type::Reference(r) => {
                                format!("&'a {} {}", r.mutability.map(|_| "mut").unwrap_or(""), r.elem.to_token_stream())
                            }
                            _ => t.ty.to_token_stream().to_string(),
                        }
                    }
                }
            )
        })
        .collect::<Vec<_>>()
        .join(", ");

    Ok(format!(
        r#"
            {vis} fn {name}_handler<T: {SYNC}>() -> {GENERIC_HANDLER}<T>
            where {type_bounds}
            {{
                struct H {{}}
                #[async_trait::async_trait]
                impl<T: {SYNC}> {HANDLER_TRAIT}<T> for H
                where {type_bounds}
                {{
                    async fn handle(&self, b: &{BOT}, u: &{UPDATE}, s: Option<&{BOT_STATE}<T>>) -> Result<(), {HANDLER_ERROR}> {{
                        {name}({}).await.map_err(|e| {{
                            {LOG}::error!("Handler {name} exited with {{:?}}", e);
                            {HANDLER_ERROR}::Runtime
                        }})
                    }}
                }}

                {GENERIC_HANDLER}{{
                    rank: {rank},
                    name: "{name}",
                    handler: Box::new(H {{}}),
                    updates: vec![{updates}],
                }}
            }}
            {}
            "#,
        item.sig
            .inputs
            .iter()
            .map(|a| {
                if let FnArg::Typed(a) = a {
                    format!("<{} as {FROM_UPDATE}<T>>::from_update(b, u, s).ok_or({HANDLER_ERROR}::Parse)?", a.ty.to_token_stream())
                } else {
                    abort!(a, "Invalid parameter")
                }
            })
            .collect::<Vec<String>>()
            .join(","),
        item.to_token_stream(),
        name = item.sig.ident
    )
    .parse()
    .unwrap())
}

pub fn try_handlers(item: TokenStream) -> Result<TokenStream, ()> {
    let handlers = Punctuated::<Expr, Token![,]>::parse_terminated.parse(item).map_err(|_| abort!(Span::call_site(), "Could not parse list"))?;
    let vec = handlers
        .into_iter()
        .map(|e| {
            if let Expr::Path(p) = e {
                format!("{}_handler()", p.to_token_stream())
            } else {
                abort!(e, "Should be a function name")
            }
        })
        .collect::<Vec<String>>()
        .join(", ");

    Ok(format!("vec![{vec}]").parse().unwrap())
}
