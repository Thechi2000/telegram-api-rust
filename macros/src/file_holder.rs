use crate::constants::*;

use proc_macro::Span;
use proc_macro::TokenStream;
use proc_macro_error::abort;

use syn::{AngleBracketedGenericArguments, GenericArgument, Item, ItemStruct, Path, PathArguments, PathSegment, Type, TypePath};

pub fn try_derive_file_holder(item: TokenStream) -> Result<TokenStream, ()> {
    // Parse the item as a structure
    let item: ItemStruct = if let Item::Struct(s) = syn::parse::<Item>(item).map_err(|_| abort!(Span::call_site(), "Could not parse"))? {
        s
    } else {
        abort!(Span::call_site(), "Can only be used on struct")
    };

    let files = item
        .fields
        .iter()
        .filter_map(|f| {
            if let Type::Path(TypePath { path: Path { segments, .. }, .. }) = &f.ty {
                if let Some(PathSegment { ident, arguments }) = segments.last() {
                    if ident == "Option" {
                        if let PathArguments::AngleBracketed(AngleBracketedGenericArguments { args, .. }) = arguments {
                            if args.len() == 1 {
                                if let GenericArgument::Type(Type::Path(TypePath { path: Path { segments, .. }, .. })) = args.first().unwrap() {
                                    if let Some(PathSegment { ident, .. }) = segments.last() {
                                        if ident == "SentFile" {
                                            return Some((true, f.ident.as_ref().unwrap().to_string()));
                                        }
                                    }
                                }
                            }
                        }
                    } else if ident == "SentFile" {
                        return Some((false, f.ident.as_ref().unwrap().to_string()));
                    }
                }
            }

            None
        })
        .collect::<Vec<(bool, String)>>();

    let impl_ = if files.is_empty() {
        format!(
            r#"impl {FILE_HOLDER} for {name} {{
                fn files(&self) -> Vec<{SENT_FILE_INFO}> {{
                    vec![]
                }}
            }}"#,
            name = item.ident
        )
    } else {
        format!(
            r#"impl {FILE_HOLDER} for {name} {{
            fn files(&self) -> Vec<{SENT_FILE_INFO}> {{
                let mut vec = vec![];
                {}
                vec
            }}
        }}"#,
            files
                .iter()
                .map(|(opt, id)| {
                    if *opt {
                        format!(r#"if let Some(file) = &self.{id} {{ if let {SENT_FILE}::Upload(file) = &file {{ vec.push({SENT_FILE_INFO} {{ field: "{id}", path: file.clone() }}); }} }}"#)
                    } else {
                        format!(r#"if let {SENT_FILE}::Upload(file) = &self.{id} {{ vec.push({SENT_FILE_INFO} {{ field: "{id}", path: file.clone() }}); }}"#)
                    }
                })
                .collect::<Vec<_>>()
                .join(";\n"),
            name = item.ident
        )
    };

    //println!("{}", impl_);
    Ok(impl_.parse().unwrap())
}
