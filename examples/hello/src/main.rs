use std::time::Duration;
use telegram_bot2::{bot, handler, BotBuilder, Bot, handlers, Builder};
use telegram_bot2::log::info;
use telegram_bot2::models::{Message, ChatId};
use telegram_bot2::models::SendMessageBuilder;

#[handler]
async fn handler(message: &Message, bot: &Bot) -> Result<(), ()> {
    bot.send_message(SendMessageBuilder::new(ChatId::from(message.chat.id), message.text.as_ref().cloned().unwrap_or("Hello !".to_string())).build()).await.unwrap();
    Ok(())
}

#[bot]
async fn bot() -> _ {
    pretty_env_logger::init();
    info!("logger inited");

    BotBuilder::new()
        .interval(Duration::from_secs(0))
        .timeout(5)
        .handlers(handlers![handler])
}
