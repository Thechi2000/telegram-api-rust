use crate::models::{InlineKeyboardMarkup, LabeledPrice, Location, MessageEntity, User};
use crate::Builder;
use serde::{Deserialize, Serialize};

/// This object represents an incoming inline query. When the user sends an empty query, your bot could return some default or trending results.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct InlineQuery {
    /// Unique identifier for this query
    pub id: String,
    /// Sender
    pub from: User,
    /// Text of the query (up to 256 characters)
    pub query: String,
    /// Offset of the results to be returned,    can be controlled by the bot
    pub offset: String,
    /// Type of the chat from which the inline query was sent.Can be either “sender” for a private chat with the inline query sender,    “private”,    “group”,    “supergroup”,    or “channel”.The chat type should be always known for requests sent from official clients and most third - party clients,    unless the request was sent from a secret chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    chat_type: Option<String>,
    /// Sender location,    only for bots that request user location
    #[serde(default, skip_serializing_if = "Option::is_none")]
    location: Option<Location>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case", tag = "type")]
/// This object represents one result of an inline query. Telegram clients currently support results of the following 20 types:
pub enum InlineQueryResult {
    /// Represents a link to an article or web page.
    Article {
        /// Unique identifier for this result, 1-64 Bytes
        id: String,
        /// Title of the result
        title: String,
        /// Content of the message to be sent
        input_message_content: InputMessageContent,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// URL of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        url: Option<String>,
        /// Pass True if you don't want the URL to be shown in the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        hide_url: Option<bool>,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Url of the thumbnail for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_url: Option<String>,
        /// Thumbnail width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_width: Option<i64>,
        /// Thumbnail height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_height: Option<i64>,
    },

    /// Represents a link to a photo. By default, this photo will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the photo.
    #[serde(rename = "photo")]
    CachedPhoto {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid URL of the photo. Photo must be in JPEG format. Photo size must not exceed 5MB
        photo_url: String,
        /// URL of the thumbnail for the photo
        thumb_url: String,
        /// Width of the photo
        #[serde(default, skip_serializing_if = "Option::is_none")]
        photo_width: Option<i64>,
        /// Height of the photo
        #[serde(default, skip_serializing_if = "Option::is_none")]
        photo_height: Option<i64>,
        /// Title for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Caption of the photo to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the photo caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the photo
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to an animated GIF file. By default, this animated GIF file will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the animation.
    #[serde(rename = "gif")]
    CachedGif {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid URL for the GIF file. File size must not exceed 1MB
        gif_url: String,
        /// Width of the GIF
        #[serde(default, skip_serializing_if = "Option::is_none")]
        gif_width: Option<i64>,
        /// Height of the GIF
        #[serde(default, skip_serializing_if = "Option::is_none")]
        gif_height: Option<i64>,
        /// Duration of the GIF in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        gif_duration: Option<i64>,
        /// URL of the static (JPEG or GIF) or animated (MPEG4) thumbnail for the result
        thumb_url: String,
        /// MIME type of the thumbnail, must be one of “image/jpeg”, “image/gif”, or “video/mp4”. Defaults to “image/jpeg”
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_mime_type: Option<String>,
        /// Title for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        /// Caption of the GIF file to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the GIF animation
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a video animation (H.264/MPEG-4 AVC video without sound). By default, this animated MPEG-4 file will be sent by the user with optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the animation.
    #[serde(rename = "mpeg4_gif")]
    CachedMpeg4Gif {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid URL for the MPEG4 file. File size must not exceed 1MB
        mpeg4_url: String,
        /// Video width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        mpeg4_width: Option<i64>,
        /// Video height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        mpeg4_height: Option<i64>,
        /// Video duration in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        mpeg4_duration: Option<i64>,
        /// URL of the static (JPEG or GIF) or animated (MPEG4) thumbnail for the result
        thumb_url: String,
        /// MIME type of the thumbnail, must be one of “image/jpeg”, “image/gif”, or “video/mp4”. Defaults to “image/jpeg”
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_mime_type: Option<String>,
        /// Title for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        /// Caption of the MPEG-4 file to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the video animation
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a page containing an embedded video player or a video file. By default, this video file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the video.
    /// If an InlineQueryResultVideo message contains an embedded video (e.g., YouTube), you must replace its content using input_message_content.
    #[serde(rename = "video")]
    CachedVideo {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid URL for the embedded video player or video file
        video_url: String,
        /// MIME type of the content of the video URL, “text/html” or “video/mp4”
        mime_type: String,
        /// URL of the thumbnail (JPEG only) for the video
        thumb_url: String,
        /// Title for the result
        title: String,
        /// Caption of the video to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the video caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Video width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        video_width: Option<i64>,
        /// Video height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        video_height: Option<i64>,
        /// Video duration in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        video_duration: Option<i64>,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the video. This field is required if InlineQueryResultVideo is used to send an HTML-page as a result (e.g., a YouTube video).
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to an MP3 audio file. By default, this audio file will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the audio.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    #[serde(rename = "audio")]
    CachedAudio {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid URL for the audio file
        audio_url: String,
        /// Title
        title: String,
        /// Caption, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the audio caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Performer
        #[serde(default, skip_serializing_if = "Option::is_none")]
        performer: Option<String>,
        /// Audio duration in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        audio_duration: Option<i64>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the audio
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a voice recording in an .OGG container encoded with OPUS. By default, this voice recording will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the the voice message.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    #[serde(rename = "voice")]
    CachedVoice {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid URL for the voice recording
        voice_url: String,
        /// Recording title
        title: String,
        /// Caption, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the voice message caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Recording duration in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        voice_duration: Option<i64>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the voice recording
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a file. By default, this file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the file. Currently, only .PDF and .ZIP files can be sent using this method.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    #[serde(rename = "document")]
    CachedDocument {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// Title for the result
        title: String,
        /// Caption of the document to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the document caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// A valid URL for the file
        document_url: String,
        /// MIME type of the content of the file, either “application/pdf” or “application/zip”
        mime_type: String,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the file
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
        /// URL of the thumbnail (JPEG only) for the file
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_url: Option<String>,
        /// Thumbnail width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_width: Option<i64>,
        /// Thumbnail height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_height: Option<i64>,
    },

    /// Represents a location on a map. By default, the location will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the location.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Location {
        /// Unique identifier for this result, 1-64 Bytes
        id: String,
        /// Location latitude in degrees
        latitude: f32,
        /// Location longitude in degrees
        longitude: f32,
        /// Location title
        title: String,
        /// The radius of uncertainty for the location, measured in meters; 0-1500
        #[serde(default, skip_serializing_if = "Option::is_none")]
        horizontal_accuracy: Option<f32>,
        /// Period in seconds for which the location can be updated, should be between 60 and 86400.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        live_period: Option<i64>,
        /// For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        heading: Option<i64>,
        /// For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        proximity_alert_radius: Option<i64>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the location
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
        /// Url of the thumbnail for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_url: Option<String>,
        /// Thumbnail width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_width: Option<i64>,
        /// Thumbnail height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_height: Option<i64>,
    },

    /// Represents a venue. By default, the venue will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the venue.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Venue {
        /// Unique identifier for this result, 1-64 Bytes
        id: String,
        /// Latitude of the venue location in degrees
        latitude: f32,
        /// Longitude of the venue location in degrees
        longitude: f32,
        /// Title of the venue
        title: String,
        /// Address of the venue
        address: String,
        /// Foursquare identifier of the venue if known
        #[serde(default, skip_serializing_if = "Option::is_none")]
        foursquare_id: Option<String>,
        /// Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
        #[serde(default, skip_serializing_if = "Option::is_none")]
        foursquare_type: Option<String>,
        /// Google Places identifier of the venue
        #[serde(default, skip_serializing_if = "Option::is_none")]
        google_place_id: Option<String>,
        /// Google Places type of the venue. (See supported types.)
        #[serde(default, skip_serializing_if = "Option::is_none")]
        google_place_type: Option<String>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the venue
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
        /// Url of the thumbnail for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_url: Option<String>,
        /// Thumbnail width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_width: Option<i64>,
        /// Thumbnail height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_height: Option<i64>,
    },

    /// Represents a contact with a phone number. By default, this contact will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the contact.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Contact {
        /// Unique identifier for this result, 1-64 Bytes
        id: String,
        /// Contact's phone number
        phone_number: String,
        /// Contact's first name
        first_name: String,
        /// Contact's last name
        #[serde(default, skip_serializing_if = "Option::is_none")]
        last_name: Option<String>,
        /// Additional data about the contact in the form of a vCard, 0-2048 bytes
        #[serde(default, skip_serializing_if = "Option::is_none")]
        vcard: Option<String>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the contact
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
        /// Url of the thumbnail for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_url: Option<String>,
        /// Thumbnail width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_width: Option<i64>,
        /// Thumbnail height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        thumb_height: Option<i64>,
    },

    /// Represents a Game.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Game {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// Short name of the game
        game_short_name: String,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
    },

    /// Represents a link to a photo stored on the Telegram servers. By default, this photo will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the photo.
    Photo {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier of the photo
        photo_file_id: String,
        /// Title for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Caption of the photo to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the photo caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the photo
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to an animated GIF file stored on the Telegram servers. By default, this animated GIF file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with specified content instead of the animation.
    Gif {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier for the GIF file
        gif_file_id: String,
        /// Title for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        /// Caption of the GIF file to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the GIF animation
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a video animation (H.264/MPEG-4 AVC video without sound) stored on the Telegram servers. By default, this animated MPEG-4 file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the animation.
    Mpeg4Gif {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier for the MPEG4 file
        mpeg4_file_id: String,
        /// Title for the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
        /// Caption of the MPEG-4 file to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the video animation
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a sticker stored on the Telegram servers. By default, this sticker will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the sticker.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Sticker {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier of the sticker
        sticker_file_id: String,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the sticker
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a file stored on the Telegram servers. By default, this file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the file.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Document {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// Title for the result
        title: String,
        /// A valid file identifier for the file
        document_file_id: String,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Caption of the document to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the document caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the file
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a video file stored on the Telegram servers. By default, this video file will be sent by the user with an optional caption. Alternatively, you can use input_message_content to send a message with the specified content instead of the video.
    Video {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier for the video file
        video_file_id: String,
        /// Title for the result
        title: String,
        /// Short description of the result
        #[serde(default, skip_serializing_if = "Option::is_none")]
        description: Option<String>,
        /// Caption of the video to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the video caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the video
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to a voice message stored on the Telegram servers. By default, this voice message will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the voice message.
    /// Note: This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
    Voice {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier for the voice message
        voice_file_id: String,
        /// Voice message title
        title: String,
        /// Caption, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the voice message caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the voice message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },

    /// Represents a link to an MP3 audio file stored on the Telegram servers. By default, this audio file will be sent by the user. Alternatively, you can use input_message_content to send a message with the specified content instead of the audio.
    Audio {
        /// Unique identifier for this result, 1-64 bytes
        id: String,
        /// A valid file identifier for the audio file
        audio_file_id: String,
        /// Caption, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the audio caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Inline keyboard attached to the message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        reply_markup: Option<InlineKeyboardMarkup>,
        /// Content of the message to be sent instead of the audio
        #[serde(default, skip_serializing_if = "Option::is_none")]
        input_message_content: Option<InputMessageContent>,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case", untagged)]
/// This object represents the content of a message to be sent as a result of an inline query
pub enum InputMessageContent {
    /// Represents the content of a text message to be sent as the result of an inline query.
    Text {
        /// Text of the message to be sent, 1-4096 characters
        message_text: String,
        /// Mode for parsing entities in the message text. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in message text, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        entities: Vec<MessageEntity>,
        /// Disables link previews for links in the sent message
        #[serde(default, skip_serializing_if = "Option::is_none")]
        disable_web_page_preview: Option<bool>,
    },

    /// Represents the content of a location message to be sent as the result of an inline query.
    Location {
        /// Latitude of the location in degrees
        latitude: f32,
        /// Longitude of the location in degrees
        longitude: f32,
        /// The radius of uncertainty for the location, measured in meters; 0-1500
        #[serde(skip_serializing_if = "Option::is_none")]
        horizontal_accuracy: Option<f32>,
        /// Period in seconds for which the location can be updated, should be between 60 and 86400.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        live_period: Option<i64>,
        /// For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        heading: Option<i64>,
        /// For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        proximity_alert_radius: Option<i64>,
    },

    /// Represents the content of a venue message to be sent as the result of an inline query.
    Venue {
        /// Latitude of the venue in degrees
        latitude: f32,
        /// Longitude of the venue in degrees
        longitude: f32,
        /// Name of the venue
        title: String,
        /// Address of the venue
        address: String,
        /// Foursquare identifier of the venue, if known
        #[serde(default, skip_serializing_if = "Option::is_none")]
        foursquare_id: Option<String>,
        /// Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
        #[serde(default, skip_serializing_if = "Option::is_none")]
        foursquare_type: Option<String>,
        /// Google Places identifier of the venue
        #[serde(default, skip_serializing_if = "Option::is_none")]
        google_place_id: Option<String>,
        /// Google Places type of the venue. (See supported types.)
        #[serde(default, skip_serializing_if = "Option::is_none")]
        google_place_type: Option<String>,
    },

    /// Represents the content of a contact message to be sent as the result of an inline query.
    Contact {
        /// Contact's phone number
        phone_number: String,
        /// Contact's first name
        first_name: String,
        /// Contact's last name
        #[serde(default, skip_serializing_if = "Option::is_none")]
        last_name: Option<String>,
        /// Additional data about the contact in the form of a vCard, 0-2048 bytes
        #[serde(default, skip_serializing_if = "Option::is_none")]
        vcard: Option<String>,
    },

    /// Represents the content of an invoice message to be sent as the result of an inline query.
    Invoice {
        /// Product name, 1-32 characters
        title: String,
        /// Product description, 1-255 characters
        description: String,
        /// Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
        payload: String,
        /// Payment provider token, obtained via @BotFather
        provider_token: String,
        /// Three-letter ISO 4217 currency code, see more on currencies
        currency: String,
        /// Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        prices: Vec<LabeledPrice>,
        /// The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0
        #[serde(default, skip_serializing_if = "Option::is_none")]
        max_tip_amount: Option<i64>,
        /// A JSON-serialized array of suggested amounts of tip in the smallest units of the currency (integer, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        suggested_tip_amounts: Vec<i64>,
        /// A JSON-serialized object for data about the invoice, which will be shared with the payment provider. A detailed description of the required fields should be provided by the payment provider.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        provider_data: Option<String>,
        /// URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        photo_url: Option<String>,
        /// Photo size in bytes
        #[serde(default, skip_serializing_if = "Option::is_none")]
        photo_size: Option<i64>,
        /// Photo width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        photo_width: Option<i64>,
        /// Photo height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        photo_height: Option<i64>,
        /// Pass True if you require the user's full name to complete the order
        #[serde(default, skip_serializing_if = "Option::is_none")]
        need_name: Option<bool>,
        /// Pass True if you require the user's phone number to complete the order
        #[serde(default, skip_serializing_if = "Option::is_none")]
        need_phone_number: Option<bool>,
        /// Pass True if you require the user's email address to complete the order
        #[serde(default, skip_serializing_if = "Option::is_none")]
        need_email: Option<bool>,
        /// Pass True if you require the user's shipping address to complete the order
        #[serde(default, skip_serializing_if = "Option::is_none")]
        need_shipping_address: Option<bool>,
        /// Pass True if the user's phone number should be sent to provider
        #[serde(default, skip_serializing_if = "Option::is_none")]
        send_phone_number_to_provider: Option<bool>,
        /// Pass True if the user's email address should be sent to provider
        #[serde(default, skip_serializing_if = "Option::is_none")]
        send_email_to_provider: Option<bool>,
        /// Pass True if the final price depends on the shipping method
        #[serde(default, skip_serializing_if = "Option::is_none")]
        is_flexible: Option<bool>,
    },
}

/// Represents a result of an inline query that was chosen by the user and sent to their chat partner.
///   Note: It is necessary to enable inline feedback via @BotFather in order to receive these objects in updates.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChosenInlineResult {
    ///    The unique identifier for the result that was chosen
    pub result_id: String,
    /// The user that chose the result
    pub from: User,
    /// Sender location, only for bots that require user location
    pub location: Option<Location>,
    ///  Identifier of the sent inline message. Available only if there is an inline keyboard attached to the message. Will be also received in callback queries and can be used to edit the message.
    pub inline_message_id: Option<String>,
    /// The query that was used to obtain the result
    pub query: String,
}

/// Describes an inline message sent by a Web App on behalf of a user.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct SentWebAppMessage {
    /// Identifier of the sent inline message.Available only if there is an inline keyboard attached to the message.
    pub inline_message_id: Option<String>,
}
