use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, Eq, PartialEq)]
#[serde(untagged)]
#[allow(missing_docs)]
pub enum ChatId {
    Integer(i64),
    String(String),
}

impl Default for ChatId {
    fn default() -> Self {
        ChatId::Integer(0)
    }
}

impl From<i64> for ChatId {
    fn from(i: i64) -> Self {
        ChatId::Integer(i)
    }
}
impl From<String> for ChatId {
    fn from(s: String) -> Self {
        ChatId::String(s)
    }
}
