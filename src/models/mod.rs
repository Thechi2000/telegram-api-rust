#![allow(dead_code)]

use crate::models::reply_markup::InlineKeyboardMarkup;
use crate::Builder;
use passport::PassportData;
use serde::{Deserialize, Serialize};

mod action;
mod chat_id;
mod chat_member;
mod command_scope;
mod inline;
mod input_media;
mod menu;
mod passport;
mod reply_markup;
mod sent_file;
mod updates;

pub use action::*;
pub use chat_id::*;
pub use chat_member::*;
pub use command_scope::*;
pub use inline::*;
pub use input_media::*;
pub use menu::*;
pub use passport::*;
pub use reply_markup::*;
pub use sent_file::*;
pub use updates::*;

#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
/// This object represents a Telegram user or bot
pub struct User {
    /// Unique identifier for this user or bot. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit i64 or double-precision f32 type are safe for storing this identifier.
    pub id: i64,
    /// True, if this user is a bot
    pub is_bot: bool,
    /// User's or bot's first name
    pub first_name: String,
    /// User's or bot's last name
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub last_name: Option<String>,
    /// User's or bot's username
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,
    /// IETF language tag of the user's language
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub language_code: Option<String>,
    /// True, if this user is a Telegram Premium user
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub is_premium: Option<bool>,
    /// True, if this user added the bot to the attachment menu
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub added_to_attachment_menu: Option<bool>,
    /// True, if the bot can be invited to groups. Returned only in getMe.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_join_groups: Option<bool>,
    /// True, if privacy mode is disabled for the bot. Returned only in getMe.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_read_all_group_messages: Option<bool>,
    /// True, if the bot supports inline queries. Returned only in getMe.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub supports_inline_queries: Option<bool>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum ChatType {
    Private,
    Group,
    SuperGroup,
    Channel,
}

/// This object represents a chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Chat {
    /// Unique identifier for this chat. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this identifier.
    pub id: i64,
    /// Type of chat, can be either “private”, “group”, “supergroup” or “channel”
    #[serde(alias = "type")]
    pub _type: ChatType,
    /// Title, for supergroups, channels and group chats
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    /// Username, for private chats, supergroups and channels if available
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,
    /// First name of the other party in a private chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub first_name: Option<String>,
    /// Last name of the other party in a private chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub last_name: Option<String>,
    /// If the supergroup chat is a forum (has topics enabled)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub is_forum: Option<bool>,
    /// Chat photo. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub photo: Option<ChatPhoto>,
    /// If non-empty, the list of all active chat usernames; for private chats, supergroups and channels. Returned only in [`get_chat`][crate::Bot::get_chat]
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    active_usernames: Vec<String>,
    /// Custom emoji identifier of emoji status of the other party in a private chat. Returned only in [`get_chat`][crate::Bot::get_chat]
    #[serde(default, skip_serializing_if = "Option::is_none")]
    emoji_status_custom_emoji_id: Option<String>,
    /// Bio of the other party in a private chat. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub bio: Option<String>,
    /// True, if privacy settings of the other party in the private chat allows to use tg://user?id=<user_id> links only in chats with the user. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_private_forwards: Option<bool>,
    /// True, if the privacy settings of the other party restrict sending voice and video note messages in the private chat. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_restricted_voice_and_video_messages: Option<bool>,
    /// True, if users need to join the supergroup before they can send messages. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub join_to_send_messages: Option<bool>,
    /// True, if all users directly joining the supergroup need to be approved by supergroup administrators. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub join_by_request: Option<bool>,
    /// Description, for groups, supergroups and channel chats. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    /// Primary invite link, for groups, supergroups and channel chats. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub invite_link: Option<String>,
    /// The most recent pinned message (by sending date). Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub pinned_message: Option<Box<Message>>,
    /// Default chat member permissions, for groups and supergroups. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub permissions: Option<ChatPermissions>,
    /// For supergroups, the minimum allowed delay between consecutive messages sent by each unpriviledged user; in seconds. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub slow_mode_delay: Option<i64>,
    /// The time after which all messages sent to the chat will be automatically deleted; in seconds. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub message_auto_delete_time: Option<i64>,
    /// True, if aggressive anti-spam checks are enabled in the supergroup. The field is only available to chat administrators. Returned only in [getChat](Bot::get_chat)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_aggressive_anti_spam_enabled: Option<bool>,
    /// True, if messages from the chat can't be forwarded to other chats. Returned only in getChat.
    /// True, if non-administrators can only get the list of bots and administrators in the chat. Returned only in [getChat](Bot::get_chat)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_hidden_members: Option<bool>,
    /// True, if messages from the chat can't be forwarded to other chats. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_protected_content: Option<bool>,
    /// For supergroups, name of group sticker set. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub sticker_set_name: Option<String>,
    /// True, if the bot can change the group sticker set. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_set_sticker_set: Option<bool>,
    /// Unique identifier for the linked chat, i.e. the discussion group identifier for a channel and vice versa; for supergroups and channel chats. This identifier may be greater than 32 bits and some programming languages may have difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit i64 or double-precision f32 type are safe for storing this identifier. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub linked_chat_id: Option<i64>,
    /// For supergroups, the location to which the supergroup is connected. Returned only in getChat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub location: Option<ChatLocation>,
}

/// This object represents a message.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Message {
    /// Unique message identifier inside this chat
    pub message_id: i64,
    /// Unique identifier of a message thread to which the message belongs; for supergroups only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub message_thread_id: Option<i64>,
    /// Sender of the message; empty for messages sent to channels. For backward compatibility, the field contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub from: Option<User>,
    /// Sender of the message, sent on behalf of a chat. For example, the channel itself for channel posts, the supergroup itself for messages from anonymous group administrators, the linked channel for messages automatically forwarded to the discussion group. For backward compatibility, the field from contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub sender_chat: Option<Box<Chat>>,
    /// Date the message was sent in Unix time
    pub date: i64,
    /// Conversation the message belongs to
    pub chat: Chat,
    /// For forwarded messages, sender of the original message
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_from: Option<User>,
    /// For messages forwarded from channels or from anonymous administrators, information about the original sender chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_from_chat: Option<Box<Chat>>,
    /// For messages forwarded from channels, identifier of the original message in the channel
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_from_message_id: Option<i64>,
    /// For forwarded messages that were originally sent in channels or by an anonymous chat administrator, signature of the message sender if present
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_signature: Option<String>,
    /// Sender's name for messages forwarded from users who disallow adding a link to their account in forwarded messages
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_sender_name: Option<String>,
    /// For forwarded messages, date the original message was sent in Unix time
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_date: Option<i64>,
    /// If the message is sent to a forum topic
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub is_topic_message: Option<i64>,
    /// True, if the message is a channel post that was automatically forwarded to the connected discussion group
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub is_automatic_forward: Option<bool>,
    /// For replies, the original message. Note that the Message object in this field will not contain further reply_to_message fields even if it itself is a reply.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub reply_to_message: Option<Box<Message>>,
    /// Bot through which the message was sent
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub via_bot: Option<User>,
    /// Date the message was last edited in Unix time
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub edit_date: Option<i64>,
    /// True, if the message can't be forwarded
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_protected_content: Option<bool>,
    /// The unique identifier of a media message group this message belongs to
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub media_group_id: Option<String>,
    /// Signature of the post author for messages in channels, or the custom title of an anonymous group administrator
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub author_signature: Option<String>,
    /// For text messages, the actual UTF-8 text of the message
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub text: Option<String>,
    /// For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub entities: Vec<MessageEntity>,
    /// Message is an animation, information about the animation. For backward compatibility, when this field is set, the document field will also be set
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub animation: Option<Animation>,
    /// Message is an audio file, information about the file
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub audio: Option<Audio>,
    /// Message is a general file, information about the file
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub document: Option<Document>,
    /// Message is a photo, available sizes of the photo
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub photo: Vec<PhotoSize>,
    /// Message is a sticker, information about the sticker
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub sticker: Option<Sticker>,
    /// Message is a video, information about the video
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub video: Option<Video>,
    /// Message is a video note, information about the video message
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub video_note: Option<VideoNote>,
    /// Message is a voice message, information about the file
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub voice: Option<Voice>,
    /// Caption for the animation, audio, document, photo, video or voice
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub caption: Option<String>,
    /// For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub caption_entities: Vec<MessageEntity>,
    /// True, if the message media is covered by a spoiler animation
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub has_media_spoiler: Option<bool>,
    /// Message is a shared contact, information about the contact
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub contact: Option<Contact>,
    /// Message is a dice with random value
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub dice: Option<Dice>,
    /// Message is a game, information about the game. More about games »
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub game: Option<Game>,
    /// Message is a native poll, information about the poll
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub poll: Option<Poll>,
    /// Message is a venue, information about the venue. For backward compatibility, when this field is set, the location field will also be set
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub venue: Option<Venue>,
    /// Message is a shared location, information about the location
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub location: Option<Location>,
    /// New members that were added to the group or supergroup and information about them (the bot itself may be one of these members)
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub new_chat_members: Vec<User>,
    /// A member was removed from the group, information about them (this member may be the bot itself)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub left_chat_member: Option<User>,
    /// A chat title was changed to this value
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub new_chat_title: Option<String>,
    /// A chat photo was change to this value
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub new_chat_photo: Vec<PhotoSize>,
    /// Service message: the chat photo was deleted
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub delete_chat_photo: Option<bool>,
    /// Service message: the group has been created
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub group_chat_created: Option<bool>,
    /// Service message: the supergroup has been created. This field can't be received in a message coming through updates, because bot can't be a member of a supergroup when it is created. It can only be found in reply_to_message if someone replies to a very first message in a directly created supergroup.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub supergroup_chat_created: Option<bool>,
    /// Service message: the channel has been created. This field can't be received in a message coming through updates, because bot can't be a member of a channel when it is created. It can only be found in reply_to_message if someone replies to a very first message in a channel.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub channel_chat_created: Option<bool>,
    /// Service message: auto-delete timer settings changed in the chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub message_auto_delete_timer_changed: Option<MessageAutoDeleteTimerChanged>,
    /// The group has been migrated to a supergroup with the specified identifier. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this identifier.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub migrate_to_chat_id: Option<i64>,
    /// The supergroup has been migrated from a group with the specified identifier. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this identifier.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub migrate_from_chat_id: Option<i64>,
    /// Specified message was pinned. Note that the Message object in this field will not contain further reply_to_message fields even if it is itself a reply.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub pinned_message: Option<Box<Message>>,
    /// Message is an invoice for a payment, information about the invoice. More about payments »
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub invoice: Option<Invoice>,
    /// Message is a service message about a successful payment, information about the payment. More about payments »
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub successful_payment: Option<SuccessfulPayment>,
    /// The domain name of the website on which the user has logged in. More about Telegram Login »
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub connected_website: Option<String>,
    /// Service message: the user allowed the bot added to the attachment menu to write messages
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub write_access_allowed: Option<WriteAccessAllowed>,
    /// Telegram Passport data
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub passport_data: Option<PassportData>,
    /// Service message. A user in the chat triggered another user's proximity alert while sharing Live Location.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub proximity_alert_triggered: Option<ProximityAlertTriggered>,
    /// Service message: forum topic created
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forum_topic_created: Option<ForumTopicCreated>,
    /// Service message: forum topic edited
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forum_topic_edited: Option<ForumTopicEdited>,
    /// Service message: forum topic closed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forum_topic_closed: Option<ForumTopicClosed>,
    /// Service message: forum topic reopened
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forum_topic_reopened: Option<ForumTopicReopened>,
    /// Service message: the 'General' forum topic hidden
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub general_forum_topic_hidden: Option<GeneralForumTopicHidden>,
    /// Service message: the 'General' forum topic unhidden
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub general_forum_topic_unhidden: Option<GeneralForumTopicUnhidden>,
    /// Service message: video chat scheduled
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub video_chat_scheduled: Option<VideoChatScheduled>,
    /// Service message: video chat started
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub video_chat_started: Option<VideoChatStarted>,
    /// Service message: video chat ended
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub video_chat_ended: Option<VideoChatEnded>,
    /// Service message: new participants invited to a video chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub video_chat_participants_invited: Option<VideoChatParticipantsInvited>,
    /// Service message: data sent by a Web App
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub web_app_data: Option<WebAppData>,
    /// Inline keyboard attached to the message. login_url buttons are represented as ordinary Url buttons.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub reply_markup: Option<InlineKeyboardMarkup>,
}

/// This object represents a unique message identifier.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct MessageId {
    /// Unique message identifier
    pub message_id: i64,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case", tag = "type")]
#[allow(missing_docs)]
pub enum MessageEntityType {
    Mention,
    Hashtag,
    CashTag,
    BotCommand,
    Url,
    Email,
    PhoneNumber,
    Bold,
    Italic,
    Underline,
    Strikethrough,
    Spoiler,
    Code,
    Pre,
    TextLink {
        url: String,
    },
    TextMention {
        user: User,
    },
    CustomEmoji {
        #[serde(rename = "custom_emoji_id")]
        id: String,
    },
}

/// This object represents one special entity in a text message. For example, hashtags, usernames, URLs, etc.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct MessageEntity {
    /// Type of the entity
    #[serde(flatten)]
    pub _type: MessageEntityType,
    /// Offset in UTF-16 code units to the start of the entity
    pub offset: i64,
    /// Length of the entity in UTF-16 code units
    pub length: i64,
}

/// This object represents one size of a photo or a file / sticker thumbnail.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct PhotoSize {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Photo width
    pub width: i64,
    /// Photo height
    pub height: i64,
    /// File size in bytes
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents an animation file (GIF or H.264/MPEG-4 AVC video without sound).
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Animation {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Video width as defined by sender
    pub width: i64,
    /// Video height as defined by sender
    pub height: i64,
    /// Duration of the video in seconds as defined by sender
    pub duration: i64,
    /// Animation thumbnail as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
    /// Original animation filename as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_name: Option<String>,
    /// MIME type of the file as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    /// File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this value.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents an audio file to be treated as music by the Telegram clients.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Audio {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Duration of the audio in seconds as defined by sender
    pub duration: i64,
    /// Performer of the audio as defined by sender or by audio tags
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub performer: Option<String>,
    /// Title of the audio as defined by sender or by audio tags
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    /// Original filename as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_name: Option<String>,
    /// MIME type of the file as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    /// File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this value.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
    /// Thumbnail of the album cover to which the music file belongs
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
}

/// This object represents a general file (as opposed to photos, voice messages and audio files).
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Document {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Document thumbnail as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
    /// Original filename as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_name: Option<String>,
    /// MIME type of the file as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    /// File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this value.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents a video file.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Video {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Video width as defined by sender
    pub width: i64,
    /// Video height as defined by sender
    pub height: i64,
    /// Duration of the video in seconds as defined by sender
    pub duration: i64,
    /// Video thumbnail
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
    /// Original filename as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_name: Option<String>,
    /// MIME type of the file as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    /// File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this value.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents a video message (available in Telegram apps as of v.4.0).
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct VideoNote {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Video width and height (diameter of the video message) as defined by sender
    pub length: i64,
    /// Duration of the video in seconds as defined by sender
    pub duration: i64,
    /// Video thumbnail
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
    /// File size in bytes
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents a voice note.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Voice {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Duration of the audio in seconds as defined by sender
    pub duration: i64,
    /// MIME type of the file as defined by sender
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    /// File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this value.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents a phone contact.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Contact {
    /// Contact's phone number
    pub phone_number: String,
    /// Contact's first name
    pub first_name: String,
    /// Contact's last name
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub last_name: Option<String>,
    /// Contact's user identifier in Telegram. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit i64 or double-precision f32 type are safe for storing this identifier.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub user_id: Option<i64>,
    /// Additional data about the contact in the form of a vCard
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub vcard: Option<String>,
}

/// This object represents an animated emoji that displays a random value.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Dice {
    /// Emoji on which the dice throw animation is based
    pub emoji: String,
    /// Value of the dice, 1-6 for “🎲”, “🎯” and “🎳” base emoji, 1-5 for “🏀” and “⚽” base emoji, 1-64 for “🎰” base emoji
    pub value: i64,
}

/// This object contains information about one answer option in a poll.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct PollOption {
    /// Option text, 1-100 characters
    pub text: String,
    /// Number of users that voted for this option
    pub voter_count: i64,
}

/// This object represents an answer of a user in a non-anonymous poll.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct PollAnswer {
    /// Unique poll identifier
    pub poll_id: String,
    /// The user, who changed the answer to the poll
    pub user: User,
    /// 0-based identifiers of answer options, chosen by the user. May be empty if the user retracted their vote.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub option_ids: Vec<i64>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum PollType {
    Regular,
    Quiz,
}

/// This object contains information about a poll.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Poll {
    /// Unique poll identifier
    pub id: String,
    /// Poll question, 1-300 characters
    pub question: String,
    /// List of poll options
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub options: Vec<PollOption>,
    /// Total number of users that voted in the poll
    pub total_voter_count: i64,
    /// True, if the poll is closed
    pub is_closed: bool,
    /// True, if the poll is anonymous
    pub is_anonymous: bool,
    /// Poll type, currently can be “regular” or “quiz”
    #[serde(alias = "type")]
    pub _type: PollType,
    /// True, if the poll allows multiple answers
    pub allows_multiple_answers: bool,
    /// 0-based identifier of the correct answer option. Available only for polls in the quiz mode, which are closed, or was sent (not forwarded) by the bot or to the private chat with the bot.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub correct_option_id: Option<i64>,
    /// Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub explanation: Option<String>,
    /// Special entities like usernames, URLs, bot commands, etc. that appear in the explanation
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub explanation_entities: Vec<MessageEntity>,
    /// Amount of time in seconds the poll will be active after creation
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub open_period: Option<i64>,
    /// Point in time (Unix timestamp) when the poll will be automatically closed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub close_date: Option<i64>,
}

/// This object represents a point on the map.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Location {
    /// Longitude as defined by sender
    pub longitude: f32,
    /// Latitude as defined by sender
    pub latitude: f32,
    /// The radius of uncertainty for the location, measured in meters; 0-1500
    pub horizontal_accuracy: f32,
    /// Time relative to the message sending date, during which the location can be updated; in seconds. For active live locations only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub live_period: Option<i64>,
    /// The direction in which user is moving, in degrees; 1-360. For active live locations only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub heading: Option<i64>,
    /// The maximum distance for proximity alerts about approaching another chat member, in meters. For sent live locations only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub proximity_alert_radius: Option<i64>,
}

/// This object represents a venue.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Venue {
    /// Venue location. Can't be a live location
    pub location: Location,
    /// Name of the venue
    pub title: String,
    /// Address of the venue
    pub address: String,
    /// Foursquare identifier of the venue
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub foursquare_id: Option<String>,
    /// Foursquare type of the venue. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub foursquare_type: Option<String>,
    /// Google Places identifier of the venue
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub google_place_id: Option<String>,
    /// Google Places type of the venue. (See supported types.)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub google_place_type: Option<String>,
}

/// Describes data sent from a Web App to the bot.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct WebAppData {
    /// The data. Be aware that a bad client can send arbitrary data in this field.
    pub data: String,
    /// Text of the web_app keyboard button from which the Web App was opened. Be aware that a bad client can send arbitrary data in this field.
    pub button_text: String,
}

/// This object represents the content of a service message, sent whenever a user in the chat triggers a proximity alert set by another user.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ProximityAlertTriggered {
    /// User that triggered the alert
    pub traveler: User,
    /// User that set the alert
    pub watcher: User,
    /// The distance between the users
    pub distance: i64,
}

/// This object represents a service message about a change in auto-delete timer settings.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct MessageAutoDeleteTimerChanged {
    /// New auto-delete time for messages in the chat; in seconds
    pub message_auto_delete_time: i64,
}

/// This object represents a service message about a video chat scheduled in the chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct VideoChatScheduled {
    /// Point in time (Unix timestamp) when the video chat is supposed to be started by a chat administrator
    pub start_date: i64,
}

/// This object represents a service message about a video chat started in the chat. Currently holds no information.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct VideoChatStarted {}

/// This object represents a service message about a video chat ended in the chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct VideoChatEnded {
    /// Video chat duration in seconds
    pub duration: i64,
}

/// This object represents a service message about new members invited to a video chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct VideoChatParticipantsInvited {
    /// New members that were invited to the video chat
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub users: Vec<User>,
}

/// This object represent a user's profile pictures.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct UserProfilePhotos {
    /// Total number of profile pictures the target user has
    pub total_count: i64,
    /// Requested profile pictures (in up to 4 sizes each)
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub photos: Vec<PhotoSize>,
}

/// This object represents a file ready to be downloaded. The file can be downloaded via the link `https://api.telegram.org/file/bot<token>/<file_path>`. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct File {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// File size in bytes. It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this value.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
    /// File path. Use `https://api.telegram.org/file/bot<token>/<file_path>` to get the file.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_path: Option<String>,
}

/// Describes a Web App.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct WebAppInfo {
    /// An HTTPS URL of a Web App to be opened with additional data as specified in Initializing Web Apps
    pub url: String,
}

/// This object represents a chat photo.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChatPhoto {
    /// File identifier of small (160x160) chat photo. This file_id can be used only for photo download and only for as long as the photo is not changed.
    pub small_file_id: String,
    /// Unique file identifier of small (160x160) chat photo, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub small_file_unique_id: String,
    /// File identifier of big (640x640) chat photo. This file_id can be used only for photo download and only for as long as the photo is not changed.
    pub big_file_id: String,
    /// Unique file identifier of big (640x640) chat photo, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub big_file_unique_id: String,
}

/// Represents an invite link for a chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChatInviteLink {
    /// The invite link. If the link was created by another chat administrator, then the second part of the link will be replaced with “…”.
    pub invite_link: String,
    /// Creator of the link
    pub creator: User,
    /// True, if users joining the chat via the link need to be approved by chat administrators
    pub creates_join_request: bool,
    /// True, if the link is primary
    pub is_primary: bool,
    /// True, if the link is revoked
    pub is_revoked: bool,
    /// Invite link name
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    /// Point in time (Unix timestamp) when the link will expire or has been expired
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub expire_date: Option<i64>,
    /// The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub member_limit: Option<i64>,
    /// Number of pending join requests created using this link
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub pending_join_request_count: Option<i64>,
}

/// Represents the rights of an administrator in a chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChatAdministratorRights {
    /// True, if the user's presence in the chat is hidden
    pub is_anonymous: bool,
    /// True, if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
    pub can_manage_chat: bool,
    /// True, if the administrator can delete messages of other users
    pub can_delete_messages: bool,
    /// True, if the administrator can manage video chats
    pub can_manage_video_chats: bool,
    /// True, if the administrator can restrict, ban or unban chat members
    pub can_restrict_members: bool,
    /// True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by the user)
    pub can_promote_members: bool,
    /// True, if the user is allowed to change the chat title, photo and other settings
    pub can_change_info: bool,
    /// True, if the user is allowed to invite new users to the chat
    pub can_invite_users: bool,
    /// True, if the administrator can post in the channel; channels only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_post_messages: Option<bool>,
    /// True, if the administrator can edit messages of other users and can pin messages; channels only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_edit_messages: Option<bool>,
    /// True, if the user is allowed to pin messages; groups and supergroups only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_pin_messages: Option<bool>,
    /// If the user is allowed to create, rename, close, and reopen forum topics; supergroups only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_manage_topics: Option<bool>,
}

/// Represents a join request sent to a chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChatJoinRequest {
    /// Chat to which the request was sent
    pub chat: Chat,
    /// User that sent the join request
    pub from: User,
    /// Date the request was sent in Unix time
    pub date: i64,
    /// Bio of the user.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub bio: Option<String>,
    /// Chat invite link that was used by the user to send the join request
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub invite_link: Option<ChatInviteLink>,
}

/// Describes actions that a non-administrator user is allowed to take in a chat.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChatPermissions {
    /// True, if the user is allowed to send text messages, contacts, locations and venues
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_send_messages: Option<bool>,
    /// True, if the user is allowed to send audios, documents, photos, videos, video notes and voice notes, implies can_send_messages
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_send_media_messages: Option<bool>,
    /// True, if the user is allowed to send polls, implies can_send_messages
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_send_polls: Option<bool>,
    /// True, if the user is allowed to send animations, games, stickers and use inline bots, implies can_send_media_messages
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_send_other_messages: Option<bool>,
    /// True, if the user is allowed to add web page previews to their messages, implies can_send_media_messages
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_add_web_page_previews: Option<bool>,
    /// True, if the user is allowed to change the chat title, photo and other settings. Ignored in public supergroups
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_change_info: Option<bool>,
    /// True, if the user is allowed to invite new users to the chat
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_invite_users: Option<bool>,
    /// True, if the user is allowed to pin messages. Ignored in public supergroups
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_pin_messages: Option<bool>,
    /// If the user is allowed to create, rename, close, and reopen forum topics; supergroups only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_manage_topics: Option<bool>,
}

/// Represents a location to which a chat is connected.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ChatLocation {
    /// The location to which the supergroup is connected. Can't be a live location.
    pub location: Location,
    /// Location address; 1-64 characters, as defined by the chat owner
    pub address: String,
}

/// This object represents a bot command.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct BotCommand {
    /// Text of the command; 1-32 characters. Can contain only lowercase English letters, digits and underscores.
    pub command: String,
    /// Description of the command; 1-256 characters.
    pub description: String,
}

/// BotCommandScopeDefault

/// If a menu button other than MenuButtonDefault is set for a private chat, then it is applied in the chat. Otherwise the default menu button is applied. By default, the menu button opens the list of bot commands.

/// Describes why a request was unsuccessful.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ResponseParameters {
    /// The group has been migrated to a supergroup with the specified identifier. This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit i64 or double-precision f32 type are safe for storing this identifier.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub migrate_to_chat_id: Option<i64>,
    /// In case of exceeding flood control, the number of seconds left to wait before the request can be repeated
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub retry_after: Option<i64>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "lowercase")]
#[allow(missing_docs)]
pub enum StickerType {
    Regular,
    Mask,
    CustomEmoji,
}

/// This object represents a sticker
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Sticker {
    /// Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    /// Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    /// Type of the sticker, currently one of “regular”, “mask”, “custom_emoji”. The type of the sticker is independent from its format, which is determined by the fields is_animated and is_video.
    #[serde(rename = "type")]
    pub _type: StickerType,
    /// Sticker width
    pub width: i64,
    /// Sticker height
    pub height: i64,
    /// True, if the sticker is animated
    pub is_animated: bool,
    /// True, if the sticker is a video sticker
    pub is_video: bool,
    /// Sticker thumbnail in the .WEBP or .JPG format
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
    /// Emoji associated with the sticker
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub emoji: Option<String>,
    /// Name of the sticker set to which the sticker belongs
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub set_name: Option<String>,
    /// For premium regular stickers, premium animation for the sticker
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub premium_animation: Option<File>,
    /// For mask stickers, the position where the mask should be placed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub mask_position: Option<MaskPosition>,
    /// For custom emoji stickers, unique identifier of the custom emoji
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub custom_emoji_id: Option<String>,
    /// File size in bytes
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub file_size: Option<i64>,
}

/// This object represents a sticker set
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct StickerSet {
    /// Sticker set name
    pub name: String,
    /// Sticker set title
    pub title: String,
    /// Type of stickers in the set, currently one of “regular”, “mask”, “custom_emoji”
    pub sticker_type: String,
    /// True, if the sticker set contains animated stickers
    pub is_animated: bool,
    /// True, if the sticker set contains video stickers
    pub is_video: bool,
    /// List of all set stickers
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub stickers: Vec<Sticker>,
    /// Sticker set thumbnail in the .WEBP, .TGS, or .WEBM format
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub thumb: Option<PhotoSize>,
}

/// This object describes the position on faces where a mask should be placed by default
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct MaskPosition {
    /// The part of the face relative to which the mask should be placed. One of “forehead”, “eyes”, “mouth”, or “chin”.
    pub point: String,
    /// Shift by X-axis measured in widths of the mask scaled to the face size, from left to right. For example, choosing -1.0 will place mask just to the left of the default mask position.
    pub x_shift: f32,
    /// Shift by Y-axis measured in heights of the mask scaled to the face size, from top to bottom. For example, 1.0 will place the mask just below the default mask position.
    pub y_shift: f32,
    /// Mask scaling coefficient. For example, 2.0 means double size.
    pub scale: f32,
}

/// This object represents a portion of the price for goods or services.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct LabeledPrice {
    /// Portion label
    pub label: String,
    /// Price of the product in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
    pub amount: i64,
}

/// This object contains basic information about an invoice.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Invoice {
    /// Product name
    pub title: String,
    /// Product description
    pub description: String,
    /// Unique bot deep-linking parameter that can be used to generate this invoice
    pub start_parameter: String,
    /// Three-letter ISO 4217 currency code
    pub currency: String,
    /// Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
    pub total_amount: i64,
}

/// This object represents a shipping address.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ShippingAddress {
    /// Two-letter ISO 3166-1 alpha-2 country code
    pub country_code: String,
    /// State, if applicable
    pub state: String,
    /// City
    pub city: String,
    /// First line for the address
    pub street_line1: String,
    /// Second line for the address
    pub street_line2: String,
    /// Address post code
    pub post_code: String,
}

/// This object represents information about an order.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct OrderInfo {
    /// User name
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    /// User's phone number
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub phone_number: Option<String>,
    /// User email
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    /// User shipping address
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub shipping_address: Option<ShippingAddress>,
}

/// This object represents one shipping option.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ShippingOption {
    /// Shipping option identifier
    pub id: String,
    /// Option title
    pub title: String,
    /// List of price portions
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub prices: Vec<LabeledPrice>,
}

/// This object contains basic information about a successful payment.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct SuccessfulPayment {
    /// Three-letter ISO 4217 currency code
    pub currency: String,
    /// Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
    pub total_amount: i64,
    /// Bot specified invoice payload
    pub invoice_payload: String,
    /// Identifier of the shipping option chosen by the user
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub shipping_option_id: Option<String>,
    /// Order information provided by the user
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub order_info: Option<OrderInfo>,
    /// Telegram payment identifier
    pub telegram_payment_charge_id: String,
    /// Provider payment identifier
    pub provider_payment_charge_id: String,
}

/// This object contains information about an incoming shipping query.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ShippingQuery {
    /// Unique query identifier
    pub id: String,
    /// User who sent the query
    pub from: User,
    /// Bot specified invoice payload
    pub invoice_payload: String,
    /// User specified shipping address
    pub shipping_address: ShippingAddress,
}

/// This object contains information about an incoming pre-checkout query.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct PreCheckoutQuery {
    /// Unique query identifier
    pub id: String,
    /// User who sent the query
    pub from: User,
    /// Three-letter ISO 4217 currency code
    pub currency: String,
    /// Total price in the smallest units of the currency (integer, not float/double). For example, for a price of US$ 1.45 pass amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
    pub total_amount: i64,
    /// Bot specified invoice payload
    pub invoice_payload: String,
    /// Identifier of the shipping option chosen by the user
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub shipping_option_id: Option<String>,
    /// Order information provided by the user
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub order_info: Option<OrderInfo>,
}

/// This object represents a game. Use BotFather to create and edit games, their short names will act as unique identifiers.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct Game {
    /// Title of the game
    pub title: String,
    /// Description of the game
    pub description: String,
    /// Photo that will be displayed in the game message in chats.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub photo: Vec<PhotoSize>,
    /// Brief description of the game or high scores included in the game message.Can be automatically edited to include current high scores for the game when the bot calls setGameScore, or manually edited using editMessageText.0 - 4096 characters
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub text: Option<String>,
    /// Special entities that appear in text, such as usernames, URLs, bot commands, etc.
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub text_entities: Vec<MessageEntity>,
    /// Animation that will be displayed in the game message in chats.Upload via BotFather
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub animation: Option<Animation>,
}

/// A placeholder, currently holds no information. Use BotFather to set up your game.
type CallbackGame = ();

/// This object represents one row of the high scores table for a game.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct GameHighScore {
    /// Position in high score table for the game
    pub position: i64,
    /// User
    pub user: User,
    /// Score
    pub score: i64,
}

/// This object represents a service message about a new forum topic created in the chat
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ForumTopicCreated {
    /// Name of the topic
    name: String,
    /// Color of the topic icon in RGB format
    icon_color: i64,
    /// Unique identifier of the custom emoji shown as the topic icon
    icon_custom_emoji_id: Option<String>,
}

/// This object represents a service message about a forum topic closed in the chat. Currently holds no information
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ForumTopicClosed {}

/// This object represents a service message about a forum topic reopened in the chat. Currently holds no information
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ForumTopicReopened {}

/// This object represents a service message about an edited forum topicF
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ForumTopicEdited {
    /// New name of the topic, if it was edited
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    /// New identifier of the custom emoji shown as the topic icon, if it was edited; an empty string if the icon was removed
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub icon_custom_emoji_id: Option<String>,
}

/// This object represents a service message about General forum topic hidden in the chat. Currently holds no information
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct GeneralForumTopicHidden {}

/// This object represents a service message about General forum topic unhidden in the chat. Currently holds no information
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct GeneralForumTopicUnhidden {}

/// This object represents a service message about a user allowing a bot added to the attachment menu to write messages. Currently holds no information
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct WriteAccessAllowed {}
