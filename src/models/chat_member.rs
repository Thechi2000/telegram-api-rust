use crate::models::{Chat, ChatInviteLink, User};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "type")]
/// This object contains information about one member of a chat
pub enum ChatMember {
    /// Represents a chat member that owns the chat and has all administrator privileges
    Owner {
        /// The member's status in the chat, always “creator”Debug, Builder, Default
        status: String,
        /// Information about the user
        user: User,
        /// True, if the user's presence in the chat is hidden
        is_anonymous: bool,
        /// . Custom title for this user
        #[serde(default, skip_serializing_if = "Option::is_none")]
        custom_title: Option<String>,
    },

    /// Represents a chat member that has some additional privileges.
    Administrator {
        /// The member's status in the chat, always “administrator”
        status: String,
        /// Information about the user
        user: User,
        /// True, if the bot is allowed to edit administrator privileges of that user
        can_be_edited: bool,
        /// True, if the user's presence in the chat is hidden
        is_anonymous: bool,
        /// True, if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
        can_manage_chat: bool,
        /// True, if the administrator can delete messages of other users
        can_delete_messages: bool,
        /// True, if the administrator can manage video chats
        can_manage_video_chats: bool,
        /// True, if the administrator can restrict, ban or unban chat members
        can_restrict_members: bool,
        /// True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by the user)
        can_promote_members: bool,
        /// True, if the user is allowed to change the chat title, photo and other settings
        can_change_info: bool,
        /// True, if the user is allowed to invite new users to the chat
        can_invite_users: bool,
        /// . True, if the administrator can post in the channel; channels only
        #[serde(default, skip_serializing_if = "Option::is_none")]
        can_post_messages: Option<bool>,
        /// . True, if the administrator can edit messages of other users and can pin messages; channels only
        #[serde(default, skip_serializing_if = "Option::is_none")]
        can_edit_messages: Option<bool>,
        /// . True, if the user is allowed to pin messages; groups and supergroups only
        #[serde(default, skip_serializing_if = "Option::is_none")]
        can_pin_messages: Option<bool>,
        /// . Custom title for this user
        #[serde(default, skip_serializing_if = "Option::is_none")]
        custom_title: Option<String>,
        /// If the user is allowed to create, rename, close, and reopen forum topics; supergroups only
        #[serde(default, skip_serializing_if = "Option::is_none")]
        can_manage_topics: Option<bool>,
    },

    /// Represents a chat member that has no additional privileges or restrictions.
    Member {
        /// The member's status in the chat, always “member”
        status: String,
        /// Information about the user
        user: User,
    },

    /// Represents a chat member that is under certain restrictions in the chat. Supergroups only.
    Restricted {
        /// The member's status in the chat, always “restricted”
        status: String,
        /// Information about the user
        user: User,
        /// True, if the user is a member of the chat at the moment of the request
        is_member: bool,
        /// True, if the user is allowed to change the chat title, photo and other settings
        can_change_info: bool,
        /// True, if the user is allowed to invite new users to the chat
        can_invite_users: bool,
        /// True, if the user is allowed to pin messages
        can_pin_messages: bool,
        /// True, if the user is allowed to send text messages, contacts, locations and venues
        can_send_messages: bool,
        /// True, if the user is allowed to send audios, documents, photos, videos, video notes and voice notes
        can_send_media_messages: bool,
        /// True, if the user is allowed to send polls
        can_send_polls: bool,
        /// True, if the user is allowed to send animations, games, stickers and use inline bots
        can_send_other_messages: bool,
        /// True, if the user is allowed to add web page previews to their messages
        can_add_web_page_previews: bool,
        /// Date when restrictions will be lifted for this user; unix time. If 0, then the user is restricted forever
        until_date: i128,
        /// If the user is allowed to create, rename, close, and reopen forum topics; supergroups only
        #[serde(default, skip_serializing_if = "Option::is_none")]
        can_manage_topics: Option<bool>,
    },

    /// Represents a chat member that isn't currently a member of the chat, but may join it themselves.
    Left {
        /// The member's status in the chat, always “left”
        status: String,
        /// Information about the user
        user: User,
    },

    /// Represents a chat member that was banned in the chat and can't return to the chat or view chat messages.
    Banned {
        /// The member's status in the chat, always “kicked”
        status: String,
        /// Information about the user
        user: User,
        /// Date when restrictions will be lifted for this user; unix time. If 0, then the user is banned forever
        until_date: i128,
    },

    /// This object represents changes in the status of a chat member.
    Updated {
        /// Chat the user belongs to
        chat: Box<Chat>,
        /// Performer of the action, which resulted in the change
        from: User,
        /// Date the change was done in Unix time
        date: i128,
        /// Previous information about the chat member
        old_chat_member: Box<ChatMember>,
        /// New information about the chat member
        new_chat_member: Box<ChatMember>,
        /// . Chat invite link, which was used by the user to join the chat; for joining by invite link events only.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        invite_link: Option<ChatInviteLink>,
    },
}
