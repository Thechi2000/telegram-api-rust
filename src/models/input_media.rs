use crate::models::MessageEntity;
use serde::{Deserialize, Serialize};

type InputFile = ();

/// This object represents the content of a media message to be sent
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum InputMedia {
    /// Represents a video to be sent.
    Video {
        /// File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files »
        media: String,
        /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More information on Sending Files »
        thumb: InputFile,
        /// . Caption of the video to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// . Mode for parsing entities in the video caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Pass True if the video needs to be covered with a spoiler animation
        #[serde(skip_serializing_if = "Option::is_none")]
        has_spoiler: Option<bool>,
        /// . Video width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        width: Option<i128>,
        /// . Video height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        height: Option<i128>,
        /// . Video duration in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        duration: Option<i128>,
        /// . Pass True if the uploaded video is suitable for streaming
        #[serde(default, skip_serializing_if = "Option::is_none")]
        supports_streaming: Option<bool>,
    },

    /// Represents an animation file (GIF or H.264/MPEG-4 AVC video without sound) to be sent.
    Animation {
        /// File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files »
        media: String,
        /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More information on Sending Files »
        thumb: InputFile,
        /// . Caption of the animation to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// . Mode for parsing entities in the animation caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Pass True if the animation needs to be covered with a spoiler animation
        #[serde(skip_serializing_if = "Option::is_none")]
        has_spoiler: Option<bool>,
        /// . Animation width
        #[serde(default, skip_serializing_if = "Option::is_none")]
        width: Option<i128>,
        /// . Animation height
        #[serde(default, skip_serializing_if = "Option::is_none")]
        height: Option<i128>,
        /// . Animation duration in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        duration: Option<i128>,
    },

    /// Represents an audio file to be treated as music to be sent.
    Audio {
        /// File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files »
        media: String,
        /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More information on Sending Files »
        thumb: InputFile,
        /// . Caption of the audio to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// . Mode for parsing entities in the audio caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// . Duration of the audio in seconds
        #[serde(default, skip_serializing_if = "Option::is_none")]
        duration: Option<i128>,
        /// . Performer of the audio
        #[serde(default, skip_serializing_if = "Option::is_none")]
        performer: Option<String>,
        /// . Title of the audio
        #[serde(default, skip_serializing_if = "Option::is_none")]
        title: Option<String>,
    },

    /// Represents a general file to be sent.
    Document {
        /// File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files »
        media: String,
        /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under <file_attach_name>. More information on Sending Files »
        thumb: InputFile,
        /// . Caption of the document to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// . Mode for parsing entities in the document caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// . Disables automatic server-side content type detection for files uploaded using multipart/form-data. Always True, if the document is sent as part of an album.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        disable_content_type_detection: Option<bool>,
    },

    /// Represents a photo to be sent
    Photo {
        /// File to send. Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://<file_attach_name>” to upload a new one using multipart/form-data under <file_attach_name> name. More information on Sending Files »
        media: String,
        /// Caption of the photo to be sent, 0-1024 characters after entities parsing
        #[serde(default, skip_serializing_if = "Option::is_none")]
        caption: Option<String>,
        /// Mode for parsing entities in the photo caption. See formatting options for more details.
        #[serde(default, skip_serializing_if = "Option::is_none")]
        parse_mode: Option<String>,
        /// List of special entities that appear in the caption, which can be specified instead of parse_mode
        #[serde(default, skip_serializing_if = "Vec::is_empty")]
        caption_entities: Vec<MessageEntity>,
        /// Pass True if the photo needs to be covered with a spoiler animation
        #[serde(skip_serializing_if = "Option::is_none")]
        has_spoiler: Option<bool>,
    },
}
