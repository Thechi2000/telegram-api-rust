use serde::{Deserialize, Serialize};

/// This object represents the scope to which bot commands are applied
#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(tag = "type")]
pub enum BotCommandScope {
    /// Represents the default scope of bot commands. Default commands are used if no commands with a narrower scope are specified for the user
    #[default]
    Default,

    /// Represents the scope of bot commands, covering all private chats
    AllPrivateChats,

    /// Represents the scope of bot commands, covering all group and supergroup chats
    AllGroupChats,

    /// Represents the scope of bot commands, covering all group and supergroup chat administrators
    AllChatAdministrator,

    /// Represents the scope of bot commands, covering a specific chat
    Chat {
        /// Unique identifier for the target chat or username of the target supergroup
        chat_id: i128,
    },

    /// Represents the scope of bot commands, covering all administrators of a specific group or supergroup chat
    ChatAdministrator {
        /// Unique identifier for the target chat or username of the target supergroup
        chat_id: i128,
    },

    /// Represents the scope of bot commands, covering a specific member of a group or supergroup chat
    ChatMember {
        /// Unique identifier for the target chat or username of the target supergroup
        chat_id: i128,
        /// Unique identifier of the target user
        user_id: i128,
    },
}
