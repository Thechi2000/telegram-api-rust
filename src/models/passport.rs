use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case", tag = "source")]
/// This object represents an error in the Telegram Passport element which was submitted that should be resolved by the user
pub enum PassportElementError {
    /// Represents an issue in one of the data fields that was provided by the user. The error is considered resolved when the field's value changes.
    Data {
        /// The section of the user's Telegram Passport which has the error
        #[serde(rename = "type")]
        _type: DataErrorType,
        /// Name of the data field which has the error
        field_name: String,
        /// Base64-encoded data hash
        data_hash: String,
        /// Error message
        message: String,
    },

    /// Represents an issue with the front side of a document. The error is considered resolved when the file with the front side of the document changes.
    FrontSide {
        /// The section of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: FrontSideErrorType,
        /// Base64-encoded hash of the file with the front side of the document
        file_hash: String,
        /// Error message
        message: String,
    },

    /// Represents an issue with the reverse side of a document. The error is considered resolved when the file with reverse side of the document changes.
    ReverseSide {
        /// The section of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: ReverseSideErrorType,
        /// Base64-encoded hash of the file with the reverse side of the document
        file_hash: String,
        /// Error message
        message: String,
    },

    /// Represents an issue with the selfie with a document. The error is considered resolved when the file with the selfie changes.
    Selfie {
        /// The section of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: SelfieErrorType,
        /// Base64-encoded hash of the file with the selfie
        file_hash: String,
        /// Error message
        message: String,
    },

    /// Represents an issue with a document scan. The error is considered resolved when the file with the document scan changes.
    File {
        /// The section of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: FileErrorType,
        /// Base64-encoded file hash
        file_hash: String,
        /// Error message
        message: String,
    },

    /// Represents an issue with a list of scans. The error is considered resolved when the list of files containing the scans changes.
    Files {
        /// The section of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: FilesErrorType,
        /// List of base64-encoded file hashes
        #[serde(skip_serializing_if = "Vec::is_empty")]
        file_hashes: Vec<String>,
        /// Error message
        message: String,
    },

    /// Represents an issue with one of the files that constitute the translation of a document. The error is considered resolved when the file changes.
    TranslationFile {
        /// Type of element of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: TranslationFileErrorType,
        /// Base64-encoded file hash
        file_hash: String,
        /// Error message
        message: String,
    },

    /// Represents an issue with the translated version of a document. The error is considered resolved when a file with the document translation change.
    TranslationFiles {
        /// Type of element of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: TranslationFilesErrorType,
        /// List of base64-encoded file hashes
        #[serde(skip_serializing_if = "Vec::is_empty")]
        file_hashes: Vec<String>,
        /// Error message
        message: String,
    },

    /// Represents an issue in an unspecified place. The error is considered resolved when new data is added.
    Unspecified {
        /// Type of element of the user's Telegram Passport which has the issue
        #[serde(rename = "type")]
        _type: String,
        /// Base64-encoded element hash
        element_hash: String,
        /// Error message
        message: String,
    },
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum DataErrorType {
    PersonalDetails,
    Passport,
    DriverLicense,
    IdentityCard,
    InternalPassport,
    Address,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum FrontSideErrorType {
    Passport,
    DriverLicense,
    IdentityCard,
    InternalPassport,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum ReverseSideErrorType {
    DriverLicense,
    IdentityCard,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum SelfieErrorType {
    Passport,
    DriverLicense,
    IdentityCard,
    InternalPassport,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum FileErrorType {
    UtilityBill,
    BankStatement,
    RentalAgreement,
    PassportRegistration,
    TemporaryRegistration,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum FilesErrorType {
    UtilityBill,
    BankStatement,
    RentalAgreement,
    PassportRegistration,
    TemporaryRegistration,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum TranslationFileErrorType {
    Passport,
    DriverLicense,
    IdentityCard,
    InternalPassport,
    UtilityBill,
    BankStatement,
    RentalAgreement,
    PassportRegistration,
    TemporaryRegistration,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum TranslationFilesErrorType {
    Passport,
    DriverLicense,
    IdentityCard,
    InternalPassport,
    UtilityBill,
    BankStatement,
    RentalAgreement,
    PassportRegistration,
    TemporaryRegistration,
}

/// Describes Telegram Passport data shared with the bot by the user.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PassportData {
    ///  Array with information about documents and other Telegram Passport elements that was shared with the bot
    pub data: Vec<EncryptedPassportElement>,
    ///  Encrypted credentials required to decrypt the data
    pub credentials: EncryptedCredentials,
}

/// This object represents a file uploaded to Telegram Passport. Currently all Telegram Passport files are in JPEG format when decrypted and don't exceed 10MB.
#[derive(Serialize, Deserialize, Clone, Debug)]
#[allow(missing_docs)]
pub struct PassportFile {
    ///  Identifier for this file, which can be used to download or reuse the file
    pub file_id: String,
    ///  Unique identifier for this file, which is supposed to be the same over time and for different bots. Can't be used to download or reuse the file.
    pub file_unique_id: String,
    ///  File size in bytes
    pub file_size: i64,
    ///  Unix time when the file was uploaded
    pub file_date: i64,
}

/// Describes data required for decrypting and authenticating EncryptedPassportElement. See the Telegram Passport Documentation for a complete description of the data decryption and authentication processes.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EncryptedCredentials {
    ///  Base64-encoded encrypted JSON-serialized data with unique user's payload, data hashes and secrets required for EncryptedPassportElement decryption and authentication
    pub data: String,
    ///  Base64-encoded data hash for data authentication
    pub hash: String,
    ///  Base64-encoded secret, encrypted with the bot's public RSA key, required for data decryption
    pub secret: String,
}

/// Describes documents or other Telegram Passport elements shared with the bot by the user.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EncryptedPassportElement {
    #[serde(flatten)]
    #[allow(missing_docs)]
    details: EncryptedPassportElementDetails,

    ///  Base64-encoded element hash for using in PassportElementErrorUnspecified
    pub hash: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "snake_case")]
#[allow(missing_docs)]
pub enum EncryptedPassportElementDetails {
    PersonalDetails {
        /// Base64-encoded encrypted Telegram Passport element data provided by the user
        data: String,
    },
    Passport {
        /// Base64-encoded encrypted Telegram Passport element data provided by the user
        data: String,
        /// Encrypted file with the front side of the document, provided by the user
        #[serde(skip_serializing_if = "Option::is_none")]
        front_side: Option<PassportFile>,
        /// Encrypted file with the selfie of the user holding a document
        selfie: PassportFile,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    DriverLicense {
        /// Base64-encoded encrypted Telegram Passport element data provided by the user
        data: String,
        /// Encrypted file with the front side of the document, provided by the user
        #[serde(skip_serializing_if = "Option::is_none")]
        front_side: Option<PassportFile>,
        /// Encrypted file with the selfie of the user holding a document
        selfie: PassportFile,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    IdentityCard {
        /// Base64-encoded encrypted Telegram Passport element data provided by the user
        data: String,
        /// Encrypted file with the front side of the document, provided by the user
        #[serde(skip_serializing_if = "Option::is_none")]
        front_side: Option<PassportFile>,
        /// Encrypted file with the selfie of the user holding a document
        selfie: PassportFile,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    InternalPassport {
        /// Base64-encoded encrypted Telegram Passport element data provided by the user
        data: String,
        /// Encrypted file with the front side of the document, provided by the user
        #[serde(skip_serializing_if = "Option::is_none")]
        front_side: Option<PassportFile>,
        /// Encrypted file with the selfie of the user holding a document
        selfie: PassportFile,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    Address {
        /// Base64-encoded encrypted Telegram Passport element data provided by the user
        data: String,
    },
    UtilityBill {
        /// Array of encrypted files with documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        files: Vec<PassportFile>,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    BankStatement {
        /// Array of encrypted files with documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        files: Vec<PassportFile>,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    RentalAgreement {
        /// Array of encrypted files with documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        files: Vec<PassportFile>,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    PassportRegistration {
        /// Array of encrypted files with documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        files: Vec<PassportFile>,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    TemporaryRegistration {
        /// Array of encrypted files with documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        files: Vec<PassportFile>,
        /// Array of encrypted files with translated versions of documents provided by the user
        #[serde(skip_serializing_if = "Vec::is_empty")]
        translation: Vec<PassportFile>,
    },
    PhoneNumber {
        /// User's verified phone number
        phone_number: String,
    },
    Email {
        /// User's verified email address
        email: String,
    },
}
