use serde::{Deserialize, Serialize};

/// Describe a file to be sent to the API.
/// There are three ways to send files (photos, stickers, audio, media, etc.):
/// - [`Id`][SentFile::Id]: If the file is already stored somewhere on the Telegram servers, you don't need to re-upload it: each file object has a file_id field, simply pass this file_id as a parameter instead of uploading. There are no limits for files sent this way.
/// - [`Url`][SentFile::Url]: Provide Telegram with an HTTP URL for the file to be sent. Telegram will download and send the file. 5 MB max size for photos and 20 MB max for other types of content.
/// - [`Upload`][SentFile::Upload]: Post the file using multipart/form-data in the usual way that files are uploaded via the browser. 10 MB max size for photos, 50 MB for other files
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum SentFile {
    /// For files already on Telegram's servers
    /// - It is not possible to change the file type when resending by file_id. I.e. a video can't be sent as a photo, a photo can't be sent as a document, etc.
    /// - It is not possible to resend thumbnails.
    /// - Resending a photo by file_id will send all of its sizes.
    /// - file_id is unique for each individual bot and can't be transferred from one bot to another.
    /// - file_id uniquely identifies a file, but a file can have different valid file_ids even for the same bot
    Id(String),
    /// File to be downloaded from a Url
    /// - The target file must have the correct MIME type (e.g., audio/mpeg for sendAudio, etc.).
    /// - sendDocument currently only works for GIF, PDF and ZIP files.
    /// - To use sendVoice, the file must have the type audio/ogg and be no more than 1MB in size. 1-20MB voice notes will be sent as files.
    /// - Other configurations may work but we can't guarantee that they will
    Url(String),
    /// Path of a local file to be uploaded to Telegram's servers. 10 MB max size for photos, 50 MB for other files
    Upload(String),
}

impl SentFile {
    /// Whether this is the [`Upload`][SentFile::Upload] variant
    pub fn is_upload(&self) -> bool {
        matches!(self, SentFile::Upload(_))
    }
}

/// Checks whether the parameters is `Some(SentFile::Upload(_))` (used for serialization skip)
pub(crate) fn skip_ser_sf_opt(f: &Option<SentFile>) -> bool {
    matches!(f, Some(SentFile::Upload(_)))
}

/// Information about a file to send to Telegram's servers
pub(crate) struct SentFileInfo {
    /// Name of the field containing the field
    pub field: &'static str,

    /// Path of the file
    pub path: String,
}

/// Describe a struct that may contain 0 to many files
pub(crate) trait FileHolder {
    /// Returns the array of [`SentFileInfo`] of the held files
    fn files(&self) -> Vec<SentFileInfo>;
}

impl FileHolder for () {
    fn files(&self) -> Vec<SentFileInfo> {
        vec![]
    }
}
