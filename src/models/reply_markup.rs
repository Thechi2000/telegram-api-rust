use crate::models::{CallbackGame, Message, User, WebAppInfo};
use crate::Builder;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
/// Specifies how the user may respond to a message
pub enum ReplyMarkup {
    /// This object represents an inline keyboard that appears right next to the message it belongs to.
    InlineKeyboard(InlineKeyboardMarkup),
    /// This object represents a custom keyboard with reply options (see Introduction to bots for details and examples).
    ReplyKeyboard(ReplyKeyboardMarkup),
    /// Upon receiving a message with this object, Telegram clients will remove the current custom keyboard and display the default letter-keyboard. By default, custom keyboards are displayed until a new keyboard is sent by a bot. An exception is made for one-time keyboards that are hidden immediately after the user presses a button (see ReplyKeyboardMarkup)
    ReplyKeyboardRemove(ReplyKeyboardRemove),
    /// Upon receiving a message with this object, Telegram clients will display a reply interface to the user (act as if the user has selected the bot's message and tapped 'Reply'). This can be extremely useful if you want to create user-friendly step-by-step interfaces without having to sacrifice privacy mode
    ForceReply(ForceReply),
}

#[derive(Serialize, Deserialize, Clone, Debug)]
/// This object represents an inline keyboard that appears right next to the message it belongs to.
pub struct InlineKeyboardMarkup {
    pub inline_keyboard: Vec<Vec<InlineKeyboardButton>>,
}

impl InlineKeyboardMarkup {
    pub fn new(buttons: Vec<Vec<InlineKeyboardButton>>) -> InlineKeyboardMarkup {
        InlineKeyboardMarkup { inline_keyboard: buttons }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
/// This object represents one button of an inline keyboard. You must use exactly one of the optional fields.
pub struct InlineKeyboardButton {
    /// Label text on the button
    pub text: String,

    /// Button type
    #[serde(flatten)]
    pub _type: InlineKeyboardButtonType,
}

/// Type of a button in an InlineKeyboardMarkup
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(untagged)]
pub enum InlineKeyboardButtonType {
    /// HTTP or tg:// URL to be opened when the button is pressed. Links tg://user?id=<user_id> can be used to mention a user by their ID without using a username, if this is allowed by their privacy settings.
    Url(String),
    /// Data to be sent in a callback query to the bot when button is pressed, 1-64 bytes
    CallbackData(String),
    /// Description of the Web App that will be launched when the user presses the button. The Web App will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery. Available only in private chats between a user and the bot.
    WebApp(WebAppInfo),
    ///  An HTTPS URL used to automatically authorize the user. Can be used as a replacement for the Telegram Login Widget.
    LoginUrl(LoginUrl),
    /// If set, pressing the button will prompt the user to select one of their chats, open that chat and insert the bot's username and the specified inline query in the input field. May be empty, in which case just the bot's username will be inserted.
    ///
    /// Note: This offers an easy way for users to start using your bot in inline mode when they are currently in a private chat with it. Especially useful when combined with switch_pm… actions - in this case the user will be automatically returned to the chat they switched from, skipping the chat selection screen.
    SwitchInlineQuery(String),

    /// If set, pressing the button will insert the bot's username and the specified inline query in the current chat's input field. May be empty, in which case only the bot's username will be inserted.
    ///
    /// This offers a quick way for the user to open your bot in inline mode in the same chat - good for selecting something from multiple options.
    SwitchInlineQueryCurrentChat(String),

    /// Description of the game that will be launched when the user presses the button.
    ///
    /// NOTE: This type of button must always be the first button in the first row.
    CallbackGame(CallbackGame),

    /// Specify True, to send a Pay button.
    ///
    /// NOTE: This type of button must always be the first button in the first row and can only be used in invoice messages.
    Pay(bool),
}

/// This object represents a custom keyboard with reply options (see Introduction to bots for details and examples).
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ReplyKeyboardMarkup {
    /// Array of button rows, each represented by an Array of KeyboardButton objects
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub keyboard: Vec<Vec<KeyboardButton>>,
    /// Requests clients to always show the keyboard when the regular keyboard is hidden. Defaults to false, in which case the custom keyboard can be hidden and opened with a keyboard icon
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub is_persistent: Option<bool>,
    /// . Requests clients to resize the keyboard vertically for optimal fit (e.g., make the keyboard smaller if there are just two rows of buttons). Defaults to false, in which case the custom keyboard is always of the same height as the app's standard keyboard.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub resize_keyboard: Option<bool>,
    /// . Requests clients to hide the keyboard as soon as it's been used. The keyboard will still be available, but clients will automatically display the usual letter-keyboard in the chat - the user can press a special button in the input field to see the custom keyboard again. Defaults to false.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub one_time_keyboard: Option<bool>,
    /// . The placeholder to be shown in the input field when the keyboard is active; 1-64 characters
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub input_field_placeholder: Option<String>,
    /// . Use this parameter if you want to show the keyboard to specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub selective: Option<bool>,
}

///fields web_app, request_contact, request_location, and request_poll are mutually exclusive.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct KeyboardButton {
    /// Text of the button. If none of the optional fields are used, it will be sent as a message when the button is pressed
    pub text: String,
    /// . If True, the user's phone number will be sent as a contact when the button is pressed. Available in private chats only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub request_contact: Option<bool>,
    /// . If True, the user's current location will be sent when the button is pressed. Available in private chats only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub request_location: Option<bool>,
    /// . If specified, the user will be asked to create a poll and send it to the bot when the button is pressed. Available in private chats only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub request_poll: Option<KeyboardButtonPollType>,
    /// . If specified, the described Web App will be launched when the button is pressed. The Web App will be able to send a “web_app_data” service message. Available in private chats only.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub web_app: Option<WebAppInfo>,
}

/// This object represents type of a poll, which is allowed to be created and sent when the corresponding button is pressed.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct KeyboardButtonPollType {
    /// . If quiz is passed, the user will be allowed to create only polls in the quiz mode. If regular is passed, only regular polls will be allowed. Otherwise, the user will be allowed to create a poll of any type.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub _type: Option<String>,
}

/// Upon receiving a message with this object, Telegram clients will remove the current custom keyboard and display the default letter-keyboard. By default, custom keyboards are displayed until a new keyboard is sent by a bot. An exception is made for one-time keyboards that are hidden immediately after the user presses a button (see ReplyKeyboardMarkup).
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ReplyKeyboardRemove {
    /// Requests clients to remove the custom keyboard (user will not be able to summon this keyboard; if you want to hide the keyboard from sight but keep it accessible, use one_time_keyboard in ReplyKeyboardMarkup)
    pub remove_keyboard: bool,
    /// . Use this parameter if you want to remove the keyboard for specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub selective: Option<bool>,
}

/// This object represents a parameter of the inline keyboard button used to automatically authorize a user. Serves as a great replacement for the Telegram Login Widget when the user is coming from Telegram. All the user needs to do is tap/click a button and confirm that they want to log in:
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct LoginUrl {
    /// An HTTPS URL to be opened with user authorization data added to the query string when the button is pressed. If the user refuses to provide authorization data, the original URL without information about the user will be opened. The data added is the same as described in Receiving authorization data.
    pub url: String,

    /// . New text of the button in forwarded messages.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub forward_text: Option<String>,
    /// . Username of a bot, which will be used for user authorization. See Setting up a bot for more details. If not specified, the current bot's username will be assumed. The Url's domain must be the same as the domain linked with the bot. See Linking your domain to the bot for more details.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub bot_username: Option<String>,
    /// . Pass True to request the permission for your bot to send messages to the user.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub request_write_access: Option<bool>,
}

/// This object represents an incoming callback query from a callback button in an inline keyboard. If the button that originated the query was attached to a message sent by the bot, the field message will be present. If the button was attached to a message sent via the bot (in inline mode), the field inline_message_id will be present. Exactly one of the fields data or game_short_name will be present.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct CallbackQuery {
    /// Unique identifier for this query
    pub id: String,
    /// Sender
    pub from: User,
    /// . Message with the callback button that originated the query. Note that message content and message date will not be available if the message is too old
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub message: Option<Message>,
    /// . Identifier of the message sent via the bot in inline mode, that originated the query.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub inline_message_id: Option<String>,
    /// Global identifier, uniquely corresponding to the chat to which the message with the callback button was sent. Useful for high scores in games.
    pub chat_instance: String,
    /// . Data associated with the callback button. Be aware that the message originated the query can contain no callback buttons with this data.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub data: Option<String>,
    /// . Short name of a Game to be returned, serves as the unique identifier for the game
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub game_short_name: Option<String>,
}

/// Upon receiving a message with this object, Telegram clients will display a reply interface to the user (act as if the user has selected the bot's message and tapped 'Reply'). This can be extremely useful if you want to create user-friendly step-by-step interfaces without having to sacrifice privacy mode.
#[derive(Serialize, Deserialize, Clone, Debug, Builder)]
pub struct ForceReply {
    /// Shows reply interface to the user, as if they manually selected the bot's message and tapped 'Reply'
    pub force_reply: bool,
    /// . The placeholder to be shown in the input field when the reply is active; 1-64 characters
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub input_field_placeholder: Option<String>,
    /// . Use this parameter if you want to force reply from specific users only. Targets: 1) users that are @mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub selective: Option<bool>,
}
