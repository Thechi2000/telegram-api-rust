use crate::models::{
    skip_ser_sf_opt, BotCommand, BotCommandScope, ChatAdministratorRights, ChatId, ChatPermissions, InlineKeyboardMarkup, InlineQueryResult, InputMedia, LabeledPrice, MenuButton, MessageEntity, PassportElementError, ReplyMarkup, SentFile,
    ShippingOption,
};
use crate::{Builder, FileHolder};
use serde::{Deserialize, Serialize};

/// Use this method to send text messages. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendMessage {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Text of the message to be sent, 1-4096 characters after entities parsing
    pub(crate) text: String,
    /// Mode for parsing entities in the message text. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in message text, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) entities: Vec<MessageEntity>,
    /// Disables link previews for links in this message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_web_page_preview: Option<bool>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to forward messages of any kind. Service messages can&#39;t be forwarded. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct ForwardMessage {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the chat where the original message was sent (or channel username in the format @channel|username)
    pub(crate) from_chat_id: ChatId,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the forwarded message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// Message identifier in the chat specified in <em>from_chat_id</em>
    pub(crate) message_id: i64,
}

/// Use this method to copy messages of any kind. Service messages and invoice messages can&#39;t be copied. A quiz [`Poll`][crate::models::Poll] can be copied only if the value of the field <em>correct_option_id</em> is known to the bot. The method is analogous to the method [`forward_message`][crate::Bot::forward_message], but the copied message doesn&#39;t have a link to the original message. Returns the [`MessageId`][crate::models::MessageId]of the sent message on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct CopyMessage {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier for the chat where the original message was sent (or channel username in the format @channel|username)
    pub(crate) from_chat_id: ChatId,
    /// Message identifier in the chat specified in <em>from_chat_id</em>
    pub(crate) message_id: i64,
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// New caption for media, 0-1024 characters after entities parsing. If not specified, the original caption is kept
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the new caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the new caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send photos. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendPhoto {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Photo to send. Pass a file_id as String to send a photo that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a photo from the Internet, or upload a new photo using multipart/form-data. The photo must be at most 10 MB in size. The photo&#39;s width and height must not exceed 10000 in total. Width and height ratio must be at most 20. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) photo: SentFile,
    /// Photo caption (may also be used when resending photos by <em>file_id</em>), 0-1024 characters after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the photo caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Pass True if the photo needs to be covered with a spoiler animation
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) has_spoiler: Option<bool>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send audio files, if you want Telegram clients to display them in the music player. Your audio must be in the .MP3 or .M4A format. On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send audio files of up to 50 MB in size, this limit may be changed in the future.</p><p>For sending voice messages, use the [`send_invoice`][crate::Bot::send_invoice] method instead.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendAudio {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Audio file to send. Pass a file_id as String to send an audio file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an audio file from the Internet, or upload a new one using multipart/form-data. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) audio: SentFile,
    /// Audio caption, 0-1024 characters after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the audio caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Duration of the audio in seconds
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) duration: Option<i64>,
    /// Performer
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) performer: Option<String>,
    /// Track name
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) title: Option<String>,
    /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail&#39;s width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can&#39;t be reused and can be only uploaded as a new file, so you can pass “attach://&lt;file_attach_name&gt;” if the thumbnail was uploaded using multipart/form-data under &lt;file_attach_name&gt;. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "skip_ser_sf_opt")]
    pub(crate) thumb: Option<SentFile>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send general files. On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send files of any type of up to 50 MB in size, this limit may be changed in the future.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendDocument {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// File to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) document: SentFile,
    /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail&#39;s width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can&#39;t be reused and can be only uploaded as a new file, so you can pass “attach://&lt;file_attach_name&gt;” if the thumbnail was uploaded using multipart/form-data under &lt;file_attach_name&gt;. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "skip_ser_sf_opt")]
    pub(crate) thumb: Option<SentFile>,
    /// Document caption (may also be used when resending documents by <em>file_id</em>), 0-1024 characters after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the document caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Disables automatic server-side content type detection for files uploaded using multipart/form-data
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_content_type_detection: Option<bool>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send video files, Telegram clients support MPEG4 videos (other formats may be sent as [`Document`][crate::models::Document]). On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendVideo {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Video to send. Pass a file_id as String to send a video that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a video from the Internet, or upload a new video using multipart/form-data. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) video: SentFile,
    /// Duration of sent video in seconds
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) duration: Option<i64>,
    /// Video width
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) width: Option<i64>,
    /// Video height
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) height: Option<i64>,
    /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail&#39;s width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can&#39;t be reused and can be only uploaded as a new file, so you can pass “attach://&lt;file_attach_name&gt;” if the thumbnail was uploaded using multipart/form-data under &lt;file_attach_name&gt;. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "skip_ser_sf_opt")]
    pub(crate) thumb: Option<SentFile>,
    /// Video caption (may also be used when resending videos by <em>file_id</em>), 0-1024 characters after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the video caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Pass True if the video needs to be covered with a spoiler animation
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) has_spoiler: Option<bool>,
    /// Pass <em>True</em> if the uploaded video is suitable for streaming
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) supports_streaming: Option<bool>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound). On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendAnimation {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Animation to send. Pass a file_id as String to send an animation that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an animation from the Internet, or upload a new animation using multipart/form-data. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) animation: SentFile,
    /// Duration of sent animation in seconds
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) duration: Option<i64>,
    /// Animation width
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) width: Option<i64>,
    /// Animation height
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) height: Option<i64>,
    /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail&#39;s width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can&#39;t be reused and can be only uploaded as a new file, so you can pass “attach://&lt;file_attach_name&gt;” if the thumbnail was uploaded using multipart/form-data under &lt;file_attach_name&gt;. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "skip_ser_sf_opt")]
    pub(crate) thumb: Option<SentFile>,
    /// Animation caption (may also be used when resending animation by <em>file_id</em>), 0-1024 characters after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the animation caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Pass True if the animation needs to be covered with a spoiler animation
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) has_spoiler: Option<bool>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message. For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as [`Audio`][crate::models::Audio] or [`Document`][crate::models::Document]). On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendVoice {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Audio file to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) voice: SentFile,
    /// Voice message caption, 0-1024 characters after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) caption: Option<String>,
    /// Mode for parsing entities in the voice message caption. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the caption, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) caption_entities: Vec<MessageEntity>,
    /// Duration of the voice message in seconds
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) duration: Option<i64>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// As of [v.4.0](https://telegram.org/blog/video-messages-and-telescope), Telegram clients support rounded square MPEG4 videos of up to 1 minute long. Use this method to send video messages. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendVideoNote {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Video note to send. Pass a file_id as String to send a video note that exists on the Telegram servers (recommended) or upload a new video using multipart/form-data. <a href="#sending-files">More information on Sending Files »</a>. Sending video notes by a URL is currently unsupported
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) video_note: SentFile,
    /// Duration of sent video in seconds
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) duration: Option<i64>,
    /// Video width and height, i.e. diameter of the video message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) length: Option<i64>,
    /// Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail&#39;s width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can&#39;t be reused and can be only uploaded as a new file, so you can pass “attach://&lt;file_attach_name&gt;” if the thumbnail was uploaded using multipart/form-data under &lt;file_attach_name&gt;. <a href="#sending-files">More information on Sending Files »</a>
    #[serde(skip_serializing_if = "skip_ser_sf_opt")]
    pub(crate) thumb: Option<SentFile>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send a group of photos, videos, documents or audios as an album. Documents and audio files can be only grouped in an album with messages of the same type. On success, an array of [`Message`][crate::models::Message] that were sent is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendMediaGroup {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// A JSON-serialized array describing messages to be sent, must include 2-10 items
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) media: Vec<InputMedia>,
    /// Sends messages <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent messages from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the messages are a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
}

/// Use this method to send point on the map. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendLocation {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Latitude of the location
    pub(crate) latitude: f64,
    /// Longitude of the location
    pub(crate) longitude: f64,
    /// The radius of uncertainty for the location, measured in meters; 0-1500
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) horizontal_accuracy: Option<f64>,
    /// Period in seconds for which the location will be updated (see <a href="https://telegram.org/blog/live-locations">Live Locations</a>, should be between 60 and 86400.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) live_period: Option<i64>,
    /// For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) heading: Option<i64>,
    /// For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) proximity_alert_radius: Option<i64>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to edit live location messages. A location can be edited until its <em>live_period</em> expires or editing is explicitly disabled by a call to [`stop_message_live_location`][crate::Bot::stop_message_live_location]. On success, if the edited message is not an inline message, the edited [`Message`][crate::models::Message] is returned, otherwise `true` is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct EditMessageLiveLocation {
    /// Required if <em>inline_message_id</em> is not specified. Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) chat_id: Option<ChatId>,
    /// Required if <em>inline_message_id</em> is not specified. Identifier of the message to edit
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_id: Option<i64>,
    /// Required if <em>chat_id</em> and <em>message_id</em> are not specified. Identifier of the inline message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) inline_message_id: Option<String>,
    /// Latitude of new location
    pub(crate) latitude: f64,
    /// Longitude of new location
    pub(crate) longitude: f64,
    /// The radius of uncertainty for the location, measured in meters; 0-1500
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) horizontal_accuracy: Option<f64>,
    /// Direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) heading: Option<i64>,
    /// The maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) proximity_alert_radius: Option<i64>,
    /// A JSON-serialized object for a new <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to stop updating a live location message before <em>live_period</em> expires. On success, if the message is not an inline message, the edited [`Message`][crate::models::Message] is returned, otherwise `true` is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct StopMessageLiveLocation {
    /// Required if <em>inline_message_id</em> is not specified. Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) chat_id: Option<ChatId>,
    /// Required if <em>inline_message_id</em> is not specified. Identifier of the message with live location to stop
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_id: Option<i64>,
    /// Required if <em>chat_id</em> and <em>message_id</em> are not specified. Identifier of the inline message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) inline_message_id: Option<String>,
    /// A JSON-serialized object for a new <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send information about a venue. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendVenue {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Latitude of the venue
    pub(crate) latitude: f64,
    /// Longitude of the venue
    pub(crate) longitude: f64,
    /// Name of the venue
    pub(crate) title: String,
    /// Address of the venue
    pub(crate) address: String,
    /// Foursquare identifier of the venue
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) foursquare_id: Option<String>,
    /// Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) foursquare_type: Option<String>,
    /// Google Places identifier of the venue
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) google_place_id: Option<String>,
    /// Google Places type of the venue. (See <a href="https://developers.google.com/places/web-service/supported_types">supported types</a>.)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) google_place_type: Option<String>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send phone contacts. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendContact {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Contact&#39;s phone number
    pub(crate) phone_number: String,
    /// Contact&#39;s first name
    pub(crate) first_name: String,
    /// Contact&#39;s last name
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) last_name: Option<String>,
    /// Additional data about the contact in the form of a <a href="https://en.wikipedia.org/wiki/VCard">vCard</a>, 0-2048 bytes
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) vcard: Option<String>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send a native poll. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendPoll {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Poll question, 1-300 characters
    pub(crate) question: String,
    /// A JSON-serialized list of answer options, 2-10 strings 1-100 characters each
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) options: Vec<String>,
    /// <em>True</em>, if the poll needs to be anonymous, defaults to <em>True</em>
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) is_anonymous: Option<bool>,
    /// Poll type, “quiz” or “regular”, defaults to “regular”
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) _type: Option<String>,
    /// <em>True</em>, if the poll allows multiple answers, ignored for polls in quiz mode, defaults to <em>False</em>
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allows_multiple_answers: Option<bool>,
    /// 0-based identifier of the correct answer option, required for polls in quiz mode
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) correct_option_id: Option<i64>,
    /// Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters with at most 2 line feeds after entities parsing
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) explanation: Option<String>,
    /// Mode for parsing entities in the explanation. See <a href="#formatting-options">formatting options</a> for more details.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) explanation_parse_mode: Option<String>,
    /// A JSON-serialized list of special entities that appear in the poll explanation, which can be specified instead of <em>parse_mode</em>
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) explanation_entities: Vec<MessageEntity>,
    /// Amount of time in seconds the poll will be active after creation, 5-600. Can&#39;t be used together with <em>close_date</em>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) open_period: Option<i64>,
    /// Point in time (Unix timestamp) when the poll will be automatically closed. Must be at least 5 and no more than 600 seconds in the future. Can&#39;t be used together with <em>open_period</em>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) close_date: Option<i64>,
    /// Pass <em>True</em> if the poll needs to be immediately closed. This can be useful for poll preview.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) is_closed: Option<bool>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method to send an animated emoji that will display a random value. On success, the sent [`Message`][crate::models::Message] is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendDice {
    /// Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i64>,
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Emoji on which the dice throw animation is based. Currently, must be one of “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB2.png" width="20" height="20" alt="🎲" />”, “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EAF.png" width="20" height="20" alt="🎯" />”, “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8F80.png" width="20" height="20" alt="🏀" />”, “<img class="emoji" src="//telegram.org/img/emoji/40/E29ABD.png" width="20" height="20" alt="⚽" />”, “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB3.png" width="20" height="20" alt="🎳" />”, or “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB0.png" width="20" height="20" alt="🎰" />”. Dice can have values 1-6 for “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB2.png" width="20" height="20" alt="🎲" />”, “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EAF.png" width="20" height="20" alt="🎯" />” and “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB3.png" width="20" height="20" alt="🎳" />”, values 1-5 for “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8F80.png" width="20" height="20" alt="🏀" />” and “<img class="emoji" src="//telegram.org/img/emoji/40/E29ABD.png" width="20" height="20" alt="⚽" />”, and values 1-64 for “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB0.png" width="20" height="20" alt="🎰" />”. Defaults to “<img class="emoji" src="//telegram.org/img/emoji/40/F09F8EB2.png" width="20" height="20" alt="🎲" />”
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) emoji: Option<String>,
    /// Sends the message <a href="https://telegram.org/blog/channels-2-0#silent-messages">silently</a>. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass <em>True</em> if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// Additional interface options. A JSON-serialized object for an <a href="https://core.telegram.org/bots#inline-keyboards-and-on-the-fly-updating">inline keyboard</a>, <a href="https://core.telegram.org/bots#keyboards">custom reply keyboard</a>, instructions to remove reply keyboard or to force a reply from the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<ReplyMarkup>,
}

/// Use this method when you need to tell the user that something is happening on the bot&#39;s side. The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status). Returns `true` on success.</p><blockquote><p>Example: The [ImageBot](https://t.me/imagebot) needs some time to process a request and upload the image. Instead of sending a text message along the lines of “Retrieving image, please wait…”, the bot may use [`send_chat_action`][crate::Bot::send_chat_action] with <em>action</em> = <em>upload_photo</em>. The user will see a “sending photo” status for the bot.</p></blockquote><p>We only recommend using this method when a response from the bot will take a <strong>noticeable</strong> amount of time to arrive.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendChatAction {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier for the target message thread; supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_thread_id: Option<i32>,
    /// Type of action to broadcast. Choose one, depending on what the user is about to receive: <em>typing</em> for <a href="#sendmessage">text messages</a>, <em>upload_photo</em> for <a href="#sendphoto">photos</a>, <em>record_video</em> or <em>upload_video</em> for <a href="#sendvideo">videos</a>, <em>record_voice</em> or <em>upload_voice</em> for <a href="#sendvoice">voice notes</a>, <em>upload_document</em> for <a href="#senddocument">general files</a>, <em>choose_sticker</em> for <a href="#sendsticker">stickers</a>, <em>find_location</em> for <a href="#sendlocation">location data</a>, <em>record_video_note</em> or <em>upload_video_note</em> for <a href="#sendvideonote">video notes</a>.
    pub(crate) action: String,
}

/// Use this method to get a list of profile pictures for a user. Returns a[`UserProfilePhotos`][crate::models::UserProfilePhotos] object.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetUserProfilePhotos {
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
    /// Sequential number of the first photo to be returned. By default, all photos are returned.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) offset: Option<i64>,
    /// Limits the number of photos to be retrieved. Values between 1-100 are accepted. Defaults to 100.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) limit: Option<i64>,
}

/// Use this method to get basic information about a file and prepare it for downloading. For the moment, bots can download files of up to 20MB in size. On success, a [`File`][crate::models::File] object is returned. The file can then be downloaded via the link `https://api.telegram.org/file/bot<token>/<file_path>`, where `<file_path>;` is taken from the response. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling [`get_file`][crate::Bot::get_file] again.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetFile {
    /// File identifier to get information about
    pub(crate) file_id: String,
}

/// Use this method to ban a user in a group, a supergroup or a channel. In the case of supergroups and channels, the user will not be able to return to the chat on their own using invite links, etc., unless [`unbanned`][crate::Bot::unban_chat_member] first. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct BanChatMember {
    /// Unique identifier for the target group or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
    /// Date when the user will be unbanned, unix time. If user is banned for more than 366 days or less than 30 seconds from the current time they are considered to be banned forever. Applied for supergroups and channels only.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) until_date: Option<i64>,
    /// Pass <em>True</em> to delete all messages from the chat for the user that is being removed. If <em>False</em>, the user will be able to see messages in the group that were sent before the user was removed. Always <em>True</em> for supergroups and channels.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) revoke_messages: Option<bool>,
}

/// Use this method to unban a previously banned user in a supergroup or channel. The user will <strong>not</strong> return to the group or channel automatically, but will be able to join via link, etc. The bot must be an administrator for this to work. By default, this method guarantees that after the call the user is not a member of the chat, but will be able to join it. So if the user is a member of the chat they will also be <strong>removed</strong> from the chat. If you don&#39;t want this, use the parameter <em>only_if_banned</em>. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct UnbanChatMember {
    /// Unique identifier for the target group or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
    /// Do nothing if the user is not banned
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) only_if_banned: Option<bool>,
}

/// Use this method to restrict a user in a supergroup. The bot must be an administrator in the supergroup for this to work and must have the appropriate administrator rights. Pass `true` for all permissions to lift restrictions from a user. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct RestrictChatMember {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroup|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
    /// A JSON-serialized object for new user permissions
    pub(crate) permissions: ChatPermissions,
    /// Date when restrictions will be lifted for the user, unix time. If user is restricted for more than 366 days or less than 30 seconds from the current time, they are considered to be restricted forever
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) until_date: Option<i64>,
}

/// Use this method to promote or demote a user in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Pass <em>False</em> for all boolean parameters to demote a user. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct PromoteChatMember {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
    /// Pass <em>True</em> if the administrator&#39;s presence in the chat is hidden
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) is_anonymous: Option<bool>,
    /// Pass <em>True</em> if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_manage_chat: Option<bool>,
    /// Pass <em>True</em> if the administrator can create channel posts, channels only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_post_messages: Option<bool>,
    /// Pass <em>True</em> if the administrator can edit messages of other users and can pin messages, channels only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_edit_messages: Option<bool>,
    /// Pass <em>True</em> if the administrator can delete messages of other users
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_delete_messages: Option<bool>,
    /// Pass <em>True</em> if the administrator can manage video chats
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_manage_video_chats: Option<bool>,
    /// Pass <em>True</em> if the administrator can restrict, ban or unban chat members
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_restrict_members: Option<bool>,
    /// Pass <em>True</em> if the administrator can add new administrators with a subset of their own privileges or demote administrators that he has promoted, directly or indirectly (promoted by administrators that were appointed by him)
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_promote_members: Option<bool>,
    /// Pass <em>True</em> if the administrator can change chat title, photo and other settings
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_change_info: Option<bool>,
    /// Pass <em>True</em> if the administrator can invite new users to the chat
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_invite_users: Option<bool>,
    /// Pass <em>True</em> if the administrator can pin messages, supergroups only
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) can_pin_messages: Option<bool>,
    /// If the user is allowed to create, rename, close, and reopen forum topics; supergroups only
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub can_manage_topics: Option<bool>,
}

/// Use this method to set a custom title for an administrator in a supergroup promoted by the bot. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatAdministratorCustomTitle {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroup|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
    /// New custom title for the administrator; 0-16 characters, emoji are not allowed
    pub(crate) custom_title: String,
}

/// Use this method to ban a channel chat in a supergroup or a channel. Until the chat is [`unbanned`][crate::Bot::unban_chat_sender_chat], the owner of the banned chat won&#39;t be able to send messages on behalf of <strong>any of their channels</strong>. The bot must be an administrator in the supergroup or channel for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct BanChatSenderChat {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target sender chat
    pub(crate) sender_chat_id: i64,
}

/// Use this method to unban a previously banned channel chat in a supergroup or channel. The bot must be an administrator for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct UnbanChatSenderChat {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target sender chat
    pub(crate) sender_chat_id: i64,
}

/// Use this method to set default chat permissions for all members. The bot must be an administrator in the group or a supergroup for this to work and must have the <em>can_restrict_members</em> administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatPermissions {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroup|username)
    pub(crate) chat_id: ChatId,
    /// A JSON-serialized object for new default chat permissions
    pub(crate) permissions: ChatPermissions,
}

/// Use this method to generate a new primary invite link for a chat; any previously generated primary link is revoked. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns the new invite link as <em>String</em> on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct ExportChatInviteLink {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to create an additional invite link for a chat. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. The link can be revoked using the method [`revoke_chat_invite_link`][crate::Bot::revoke_chat_invite_link]. Returns the new invite link as [`ChatInviteLink`][crate::models::ChatInviteLink] object.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct CreateChatInviteLink {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Invite link name; 0-32 characters
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) name: Option<String>,
    /// Point in time (Unix timestamp) when the link will expire
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) expire_date: Option<i64>,
    /// The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) member_limit: Option<i64>,
    /// <em>True</em>, if users joining the chat via the link need to be approved by chat administrators. If <em>True</em>, <em>member_limit</em> can&#39;t be specified
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) creates_join_request: Option<bool>,
}

/// Use this method to edit a non-primary invite link created by the bot. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns the edited invite link as a [`ChatInviteLink`][crate::models::ChatInviteLink] object.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct EditChatInviteLink {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// The invite link to edit
    pub(crate) invite_link: String,
    /// Invite link name; 0-32 characters
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) name: Option<String>,
    /// Point in time (Unix timestamp) when the link will expire
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) expire_date: Option<i64>,
    /// The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) member_limit: Option<i64>,
    /// <em>True</em>, if users joining the chat via the link need to be approved by chat administrators. If <em>True</em>, <em>member_limit</em> can&#39;t be specified
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) creates_join_request: Option<bool>,
}

/// Use this method to revoke an invite link created by the bot. If the primary link is revoked, a new link is automatically generated. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns the revoked invite link as [`ChatInviteLink`][crate::models::ChatInviteLink] object.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct RevokeChatInviteLink {
    /// Unique identifier of the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// The invite link to revoke
    pub(crate) invite_link: String,
}

/// Use this method to approve a chat join request. The bot must be an administrator in the chat for this to work and must have the <em>can_invite_users</em> administrator right. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct ApproveChatJoinRequest {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
}

/// Use this method to decline a chat join request. The bot must be an administrator in the chat for this to work and must have the <em>can_invite_users</em> administrator right. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct DeclineChatJoinRequest {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
}

/// Use this method to set a new profile photo for the chat. Photos can&#39;t be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatPhoto {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// New chat photo, uploaded using multipart/form-data
    #[serde(skip_serializing_if = "SentFile::is_upload")]
    pub(crate) photo: SentFile,
}

/// Use this method to delete a chat photo. Photos can&#39;t be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct DeleteChatPhoto {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to change the title of a chat. Titles can&#39;t be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatTitle {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// New chat title, 1-255 characters
    pub(crate) title: String,
}

/// Use this method to change the description of a group, a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatDescription {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// New chat description, 0-255 characters
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) description: Option<String>,
}

/// Use this method to add a message to the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the &#39;can_pin_messages&#39; administrator right in a supergroup or &#39;can_edit_messages&#39; administrator right in a channel. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct PinChatMessage {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Identifier of a message to pin
    pub(crate) message_id: i64,
    /// Pass <em>True</em> if it is not necessary to send a notification to all chat members about the new pinned message. Notifications are always disabled in channels and private chats.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
}

/// Use this method to remove a message from the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the &#39;can_pin_messages&#39; administrator right in a supergroup or &#39;can_edit_messages&#39; administrator right in a channel. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct UnpinChatMessage {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Identifier of a message to unpin. If not specified, the most recent pinned message (by sending date) will be unpinned.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) message_id: Option<i64>,
}

/// Use this method to clear the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the &#39;can_pin_messages&#39; administrator right in a supergroup or &#39;can_edit_messages&#39; administrator right in a channel. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct UnpinAllChatMessages {
    /// Unique identifier for the target chat or username of the target channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method for your bot to leave a group, supergroup or channel. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct LeaveChat {
    /// Unique identifier for the target chat or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to get up to date information about the chat (current name of the user for one-on-one conversations, current username of a user, group or channel, etc.). Returns a [`Chat`][crate::models::Chat] object on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetChat {
    /// Unique identifier for the target chat or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to get a list of administrators in a chat, which aren&#39;t bots. Returns an Array of [`ChatMember`][crate::models::ChatMember] objects.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetChatAdministrators {
    /// Unique identifier for the target chat or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to get the number of members in a chat. Returns <em>Int</em> on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetChatMemberCount {
    /// Unique identifier for the target chat or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to get information about a member of a chat. Returns a [`ChatMember`][crate::models::ChatMember] object on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetChatMember {
    /// Unique identifier for the target chat or username of the target supergroup or channel (in the format @channel|username)
    pub(crate) chat_id: ChatId,
    /// Unique identifier of the target user
    pub(crate) user_id: i64,
}

/// Use this method to set a new group sticker set for a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Use the field <em>can_set_sticker_set</em> optionally returned in [`get_chat`][crate::Bot::get_chat] requests to check if the bot can use this method. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatStickerSet {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroup|username)
    pub(crate) chat_id: ChatId,
    /// Name of the sticker set to be set as the group sticker set
    pub(crate) sticker_set_name: String,
}

/// Use this method to delete a group sticker set from a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Use the field <em>can_set_sticker_set</em> optionally returned in [`get_chat`][crate::Bot::get_chat] requests to check if the bot can use this method. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct DeleteChatStickerSet {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroup|username)
    pub(crate) chat_id: ChatId,
}

/// Use this method to send answers to callback queries sent from [`inline keyboards`](https://core.telegram.org/bots/features#inline-keyboards). The answer will be displayed to the user as a notification at the top of the chat screen or as an alert. On success, `true` is returned.</p><blockquote><p>Alternatively, the user can be redirected to the specified Game URL. For this option to work, you must first create a game for your bot via [@BotFather](https://t.me/botfather) and accept the terms. Otherwise, you may use links like <code>t.me/your_bot?start=XXXX</code> that open your bot with a parameter
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct AnswerCallbackQuery {
    /// A JSON-serialized list of bot commands to be set as the list of the bot&#39;s commands. At most 100 commands can be specified.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) commands: Vec<BotCommand>,
    /// A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to <a href="#botcommandscopedefault">BotCommandScopeDefault</a>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) scope: Option<BotCommandScope>,
    /// A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) language_code: Option<String>,
}

/// Use this method to change the list of the bot's commands. See this manual for more details about bot commands. Returns `true` on success
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetMyCommands {
    /// A JSON-serialized list of bot commands to be set as the list of the bot's commands. At most 100 commands can be specified.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) commands: Vec<BotCommand>,
    /// A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to BotCommandScopeDefault.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) scope: Option<BotCommandScope>,
    /// A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) language_code: Option<String>,
}

/// Use this method to delete the list of the bot&#39;s commands for the given scope and user language. After deletion, [higher level commands](https://core.telegram.org/bots/api#determining-list-of-commands) will be shown to affected users. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct DeleteMyCommands {
    /// A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to <a href="#botcommandscopedefault">BotCommandScopeDefault</a>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) scope: Option<BotCommandScope>,
    /// A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) language_code: Option<String>,
}

/// Use this method to get the current list of the bot&#39;s commands for the given scope and user language. Returns an Array of [`BotCommand`][crate::models::BotCommand] objects. If commands aren&#39;t set, an empty list is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetMyCommands {
    /// A JSON-serialized object, describing scope of users. Defaults to <a href="#botcommandscopedefault">BotCommandScopeDefault</a>.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) scope: Option<BotCommandScope>,
    /// A two-letter ISO 639-1 language code or an empty string
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) language_code: Option<String>,
}

/// Use this method to change the bot&#39;s menu button in a private chat, or the default menu button. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetChatMenuButton {
    /// Unique identifier for the target private chat. If not specified, default bot&#39;s menu button will be changed
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) chat_id: Option<i64>,
    /// A JSON-serialized object for the bot&#39;s new menu button. Defaults to <a href="#menubuttondefault">MenuButtonDefault</a>
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) menu_button: Option<MenuButton>,
}

/// Use this method to get the current value of the bot&#39;s menu button in a private chat, or the default menu button. Returns [`MenuButton`][crate::models::MenuButton] on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetChatMenuButton {
    /// Unique identifier for the target private chat. If not specified, default bot&#39;s menu button will be returned
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) chat_id: Option<i64>,
}

/// Use this method to change the default administrator rights requested by the bot when it&#39;s added as an administrator to groups or channels. These rights will be suggested to users, but they are are free to modify the list before adding the bot. Returns `true` on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetMyDefaultAdministratorRights {
    /// A JSON-serialized object describing new default administrator rights. If not specified, the default administrator rights will be cleared.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) rights: Option<ChatAdministratorRights>,
    /// Pass <em>True</em> to change the default administrator rights of the bot in channels. Otherwise, the default administrator rights of the bot for groups and supergroups will be changed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) for_channels: Option<bool>,
}

/// Use this method to get the current default administrator rights of the bot. Returns [`ChatAdministratorRights`][crate::models::ChatAdministratorRights] on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetMyDefaultAdministratorRights {
    /// Pass <em>True</em> to get default administrator rights of the bot in channels. Otherwise, default administrator rights of the bot for groups and supergroups will be returned.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) for_channels: Option<bool>,
}

/// Use this method to send invoices. On success, the sent Message is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendInvoice {
    /// Unique identifier for the target chat or username of the target channel (in the format @channelusername)
    pub(crate) chat_id: ChatId,
    /// Product name, 1-32 characters
    pub(crate) title: String,
    /// Product description, 1-255 characters
    pub(crate) description: String,
    /// Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
    pub(crate) payload: String,
    /// Payment provider token, obtained via @BotFather
    pub(crate) provider_token: String,
    /// Three-letter ISO 4217 currency code, see more on currencies
    pub(crate) currency: String,
    /// Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) prices: Vec<LabeledPrice>,
    /// The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) max_tip_amount: Option<i64>,
    /// A JSON-serialized array of suggested amounts of tips in the smallest units of the currency (integer, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) suggested_tip_amounts: Vec<i64>,
    /// Unique deep-linking parameter. If left empty, forwarded copies of the sent message will have a Pay button, allowing multiple users to pay directly from the forwarded message, using the same invoice. If non-empty, forwarded copies of the sent message will have a URL button with a deep link to the bot (instead of a Pay button), with the value used as the start parameter
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) start_parameter: Option<String>,
    /// JSON-serialized data about the invoice, which will be shared with the payment provider. A detailed description of required fields should be provided by the payment provider.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) provider_data: Option<String>,
    /// URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service. People like it better when they see what they are paying for.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_url: Option<String>,
    /// Photo size in bytes
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_size: Option<i64>,
    /// Photo width
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_width: Option<i64>,
    /// Photo height
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_height: Option<i64>,
    /// Pass True if you require the user's full name to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_name: Option<bool>,
    /// Pass True if you require the user's phone number to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_phone_number: Option<bool>,
    /// Pass True if you require the user's email address to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_email: Option<bool>,
    /// Pass True if you require the user's shipping address to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_shipping_address: Option<bool>,
    /// Pass True if the user's phone number should be sent to provider
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) send_phone_number_to_provider: Option<bool>,
    /// Pass True if the user's email address should be sent to provider
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) send_email_to_provider: Option<bool>,
    /// Pass True if the final price depends on the shipping method
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) is_flexible: Option<bool>,
    /// Sends the message silently. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass True if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// A JSON-serialized object for an inline keyboard. If empty, one 'Pay total price' button will be shown. If not empty, the first button must be a Pay button.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<InlineKeyboardMarkup>,
}

/// Use this method to create a link for an invoice. Returns the created invoice link as String on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct CreateInvoiceLink {
    /// Product name, 1-32 characters
    pub(crate) title: String,
    /// Product description, 1-255 characters
    pub(crate) description: String,
    /// Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
    pub(crate) payload: String,
    /// Payment provider token, obtained via BotFather
    pub(crate) provider_token: String,
    /// Three-letter ISO 4217 currency code, see more on currencies
    pub(crate) currency: String,
    /// Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.)
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) prices: Vec<LabeledPrice>,
    /// The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) max_tip_amount: Option<i64>,
    /// A JSON-serialized array of suggested amounts of tips in the smallest units of the currency (integer, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) suggested_tip_amounts: Vec<i64>,
    /// JSON-serialized data about the invoice, which will be shared with the payment provider. A detailed description of required fields should be provided by the payment provider.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) provider_data: Option<String>,
    /// URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_url: Option<String>,
    /// Photo size in bytes
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_size: Option<i64>,
    /// Photo width
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_width: Option<i64>,
    /// Photo height
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) photo_height: Option<i64>,
    /// Pass True if you require the user's full name to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_name: Option<bool>,
    /// Pass True if you require the user's phone number to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_phone_number: Option<bool>,
    /// Pass True if you require the user's email address to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_email: Option<bool>,
    /// Pass True if you require the user's shipping address to complete the order
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) need_shipping_address: Option<bool>,
    /// Pass True if the user's phone number should be sent to the provider
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) send_phone_number_to_provider: Option<bool>,
    /// Pass True if the user's email address should be sent to the provider
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) send_email_to_provider: Option<bool>,
    /// Pass True if the final price depends on the shipping method
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) is_flexible: Option<bool>,
}

/// If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will send an Update with a shipping_query field to the bot. Use this method to reply to shipping queries. On success, True is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct AnswerShippingQuery {
    /// Unique identifier for the query to be answered
    pub(crate) shipping_query_id: String,
    /// Pass True if delivery to the specified address is possible and False if there are any problems (for example, if delivery to the specified address is not possible)
    pub(crate) ok: bool,
    /// Required if ok is True. A JSON-serialized array of available shipping options.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) shipping_options: Vec<ShippingOption>,
    /// Required if ok is False. Error message in human readable form that explains why it is impossible to complete the order (e.g. "Sorry, delivery to your desired address is unavailable'). Telegram will display this message to the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) error_message: Option<String>,
}

/// Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the form of an Update with the field pre_checkout_query. Use this method to respond to such pre-checkout queries. On success, True is returned. Note: The Bot API must receive an answer within 10 seconds after the pre-checkout query was sent.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct AnswerPreCheckoutQuery {
    /// Unique identifier for the query to be answered
    pub(crate) pre_checkout_query_id: String,
    /// Specify True if everything is alright (goods are available, etc.) and the bot is ready to proceed with the order. Use False if there are any problems.
    pub(crate) ok: bool,
    /// Required if ok is False. Error message in human readable form that explains the reason for failure to proceed with the checkout (e.g. "Sorry, somebody just bought the last of our amazing black T-shirts while you were busy filling out your payment details. Please choose a different color or garment!"). Telegram will display this message to the user.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) error_message: Option<String>,
}

/// Informs a user that some of the Telegram Passport elements they provided contains errors. The user will not be able to re-submit their Passport to you until the errors are fixed (the contents of the field for which you returned the error must change). Returns True on success.
///
/// Use this if the data submitted by the user doesn't satisfy the standards your service requires for any reason. For example, if a birthday date seems invalid, a submitted document is blurry, a scan shows evidence of tampering, etc. Supply some details in the error message to make sure the user knows how to correct the issues.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetPasswordDataErrors {
    user_id: i64,
    errors: Vec<PassportElementError>,
}

/// Use this method to send a game. On success, the sent Message is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SendGame {
    /// Unique identifier for the target chat
    pub(crate) chat_id: i64,
    /// Short name of the game, serves as the unique identifier for the game. Set up your games via @BotFather.
    pub(crate) game_short_name: String,
    /// Sends the message silently. Users will receive a notification with no sound.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_notification: Option<bool>,
    /// Protects the contents of the sent message from forwarding and saving
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) protect_content: Option<bool>,
    /// If the message is a reply, ID of the original message
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_to_message_id: Option<i64>,
    /// Pass True if the message should be sent even if the specified replied-to message is not found
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) allow_sending_without_reply: Option<bool>,
    /// A JSON-serialized object for an inline keyboard. If empty, one 'Play game_title' button will be shown. If not empty, the first button must launch the game.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) reply_markup: Option<InlineKeyboardMarkup>,
}

/// Use this method to set the score of the specified user in a game message. On success, if the message is not an inline message, the Message is returned, otherwise True is returned. Returns an error, if the new score is not greater than the user's current score in the chat and force is False.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct SetGameScore {
    /// User identifier
    pub(crate) user_id: i64,
    /// New score, must be non-negative
    pub(crate) score: i64,
    /// Pass True if the high score is allowed to decrease. This can be useful when fixing mistakes or banning cheaters
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) force: Option<bool>,
    /// Pass True if the game message should not be automatically edited to include the current scoreboard
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) disable_edit_message: Option<bool>,

    #[serde(flatten)]
    message_id: GameScoreMessageId,
}

/// Use this method to get data for high score tables. Will return the score of the specified user and several of their neighbors in a game. Returns an Array of GameHighScore objects.
///
/// This method will currently return scores for the target user, plus two of their closest neighbors on each side. Will also return the top three users if the user and their neighbors are not among them. Please note that this behavior is subject to change.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct GetGameHighScores {
    /// Target user id
    pub(crate) user_id: i64,

    #[serde(flatten)]
    message_id: GameScoreMessageId,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[allow(missing_docs)]
pub enum GameScoreMessageId {
    Inline {
        /// Identifier of the inline message
        inline_message_id: String,
    },
    NotInline {
        /// Required if inline_message_id is not specified. Unique identifier for the target chat
        chat_id: i64,
        /// Required if inline_message_id is not specified. Identifier of the sent message
        message_id: i64,
    },
}

/// Use this method to send answers to an inline query. On success, True is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct AnswerInlineQuery {
    /// Unique identifier for the answered query
    pub(crate) inline_query_id: String,
    /// A JSON-serialized array of results for the inline query
    /// No more than 50 results per query are allowed.
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub(crate) results: Vec<InlineQueryResult>,
    /// The maximum amount of time in seconds that the result of the inline query may be cached on the server. Defaults to 300.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) cache_time: Option<i64>,
    /// Pass True if results may be cached on the server side only for the user that sent the query. By default, results may be returned to any user who sends the same query
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) is_personal: Option<bool>,
    /// Pass the offset that a client should send in the next query with the same text to receive more results. Pass an empty string if there are no more results or if you don't support pagination. Offset length can't exceed 64 bytes.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) next_offset: Option<String>,
    /// If passed, clients will display a button with specified text that switches the user to a private chat with the bot and sends the bot a start message with the parameter switch_pm_parameter
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) switch_pm_text: Option<String>,
    /// Deep-linking parameter for the /start message sent to the bot when user presses the switch button. 1-64 characters, only A-Z, a-z, 0-9, _ and - are allowed.
    ///
    /// Example: An inline bot that sends YouTube videos can ask the user to connect the bot to their YouTube account to adapt search results accordingly. To do this, it displays a 'Connect your YouTube account' button above the results, or even before showing any. The user presses the button, switches to a private chat with the bot and, in doing so, passes a start parameter that instructs the bot to return an OAuth link. Once done, the bot can offer a switch_inline button so that the user can easily return to the chat where they wanted to use the bot's inline capabilities.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub(crate) switch_pm_parameter: Option<String>,
}

/// Use this method to set the result of an interaction with a Web App and send a corresponding message on behalf of the user to the chat from which the query originated. On success, a SentWebAppMessage object is returned.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct AnswerWebAppQuery {
    /// Unique identifier for the query to be answered
    pub(crate) web_app_query_id: String,
    /// A JSON-serialized object describing the message to be sent
    pub(crate) result: InlineQueryResult,
}

/// Use this method to create a topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. Returns information about the created topic as a ForumTopic object.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct CreateForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    chat_id: ChatId,
    /// Topic name, 1-128 characters
    name: String,
    /// Color of the topic icon in RGB format. Currently, must be one of 0x6FB9F0, 0xFFD67E, 0xCB86DB, 0x8EEE98, 0xFF93B2, or 0xFB6F5F
    #[serde(default, skip_serializing_if = "Option::is_none")]
    icon_color: Option<i64>,
    /// Unique identifier of the custom emoji shown as the topic icon. Use getForumTopicIconStickers to get all allowed custom emoji identifiers.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    icon_custom_emoji_id: Option<String>,
}

/// Use this method to edit name and icon of a topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have can_manage_topics administrator rights, unless it is the creator of the topic. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct EditForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    #[serde(default, skip_serializing_if = "Option::is_none")]
    chat_id: Option<ChatId>,
    /// Unique identifier for the target message thread of the forum topic
    message_thread_id: i64,
    /// New topic name, 1-128 characters
    name: String,
    /// New unique identifier of the custom emoji shown as the topic icon. Use getForumTopicIconStickers to get all allowed custom emoji identifiers.
    #[serde(default, skip_serializing_if = "Option::is_none")]
    icon_custom_emoji_id: Option<String>,
}

/// Use this method to close an open topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights, unless it is the creator of the topic. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct CloseForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    chat_id: ChatId,
    /// Unique identifier for the target message thread of the forum topic
    message_thread_id: i64,
}

/// Use this method to reopen a closed topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights, unless it is the creator of the topic. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct ReopenForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    chat_id: ChatId,
    /// Unique identifier for the target message thread of the forum topic
    message_thread_id: i64,
}

/// Use this method to delete a forum topic along with all its messages in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_delete_messages administrator rights. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct DeleteForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    chat_id: ChatId,
    /// Unique identifier for the target message thread of the forum topic
    message_thread_id: i64,
}

/// Use this method to clear the list of pinned messages in a forum topic. The bot must be an administrator in the chat for this to work and must have the can_pin_messages administrator right in the supergroup. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct UnpinAllForumTopicMessages {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    chat_id: ChatId,
    /// Unique identifier for the target message thread of the forum topic
    message_thread_id: i64,
}


/// Use this method to edit the name of the 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have can_manage_topics administrator rights. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct EditGeneralForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup (in the format @supergroupusername)
    chat_id: ChatId,
    /// New topic name, 1-128 characters
    name: String,
}

/// Use this method to close an open 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct CloseGeneralForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup ( in the format @ supergroupusername)
    chat_id: ChatId,
}

/// Use this method to reopen a closed 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. The topic will be automatically unhidden if it was hidden. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct ReopenGeneralForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup ( in the format @ supergroupusername)
    chat_id: ChatId,
}

/// Use this method to hide the 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. The topic will be automatically closed if it was open. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct HideGeneralForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup ( in the format @ supergroupusername)
    chat_id: ChatId,
}

/// Use this method to unhide the 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. Returns True on success.
#[derive(Serialize, Deserialize, Clone, Debug, Builder, FileHolder)]
pub struct UnhideGeneralForumTopic {
    /// Unique identifier for the target chat or username of the target supergroup ( in the format @ supergroupusername)
    chat_id: ChatId,
}