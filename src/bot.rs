use crate::error::{parse_api_result, Error};
use crate::models::*;
use reqwest::multipart::{Form, Part};
use reqwest::Client;
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_json::Value;
use std::path::PathBuf;
use tokio::io::AsyncReadExt;

fn generate_request(token: &str, request: &str) -> String {
    format!("https://api.telegram.org/bot{}/{}", token, request)
}

/// The Bot struct is used as a handle to send request to the Telegram Api
///
/// You may at any time refer to <https://core.telegram.org/bots/api> for the full documentation
#[derive(Clone)]
pub struct Bot {
    pub(crate) token: String,
    pub(crate) client: Client,
}

impl Bot {
    pub(crate) fn login(token: String) -> Bot {
        Bot { token, client: Client::new() }
    }

    async fn send_request<'a, B: Serialize + FileHolder, A: DeserializeOwned>(&self, url: &str, body: Option<B>) -> Result<A, Error> {
        let request = self.client.post(generate_request(self.token.as_str(), url));

        let request = if let Some(body) = body {
            let value = serde_json::to_value(&body)?;
            let mut form = Form::new();

            if let Value::Object(map) = &value {
                for (key, value) in map {
                    form = form.text(key.clone(), if let Value::String(s) = value { s.clone() } else { serde_json::to_string(value)? })
                }
            }

            for f in body.files() {
                let path: PathBuf = f.path.into();

                let mut buf = vec![];
                tokio::fs::File::open(&path).await?.read_to_end(&mut buf).await?;

                form = form.part(f.field, Part::bytes(buf).file_name(path.file_name().unwrap().to_str().unwrap().to_string()))
            }

            request.multipart(form)
        } else {
            request
        };

        parse_api_result(request.send().await?.text().await?)
    }

    pub(crate) async fn get_updates(&self, body: GetUpdates) -> Result<Vec<UpdateStruct>, Error> {
        self.send_request("getUpdates", Some(body)).await
    }

    /// A simple method for testing your bot's authentication token. Requires no parameters. Returns basic information about the bot in form of a [`User`][crate::models::User] object
    pub async fn get_me(&self) -> Result<User, Error> {
        self.send_request::<(), User>("getMe", None).await
    }

    /// Use this method to log out from the cloud Bot API server before launching the bot locally. You must log out the bot before running it locally, otherwise there is no guarantee that the bot will receive updates. After a successful call, you can immediately log in on a local server, but will not be able to log in back to the cloud Bot API server for 10 minutes. Returns `true` on success.
    pub async fn log_out(&self) -> Result<bool, Error> {
        self.send_request::<(), bool>("logOut", None).await
    }

    /// Use this method to close the bot instance before moving it from one local server to another. You need to delete the webhook before calling this method to ensure that the bot isn't launched again after server restart. The method will return error 429 in the first 10 minutes after the bot is launched. Returns `true` on success
    pub async fn close(&self) -> Result<bool, Error> {
        self.send_request::<(), bool>("close", None).await
    }

    /// Use this method to send text messages. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_message(&self, body: SendMessage) -> Result<Message, Error> {
        self.send_request("sendMessage", Some(body)).await
    }

    /// Use this method to forward messages of any kind. Service messages can&#39;t be forwarded. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn forward_message(&self, body: ForwardMessage) -> Result<Message, Error> {
        self.send_request("forwardMessage", Some(body)).await
    }

    /// Use this method to copy messages of any kind. Service messages and invoice messages can&#39;t be copied. A quiz [`Poll`][crate::models::Poll] can be copied only if the value of the field <em>correct_option_id</em> is known to the bot. The method is analogous to the method [`forward_message`][Self::forward_message], but the copied message doesn&#39;t have a link to the original message. Returns the [`MessageId`][crate::models::MessageId]of the sent message on success.
    pub async fn copy_message(&self, body: CopyMessage) -> Result<Message, Error> {
        self.send_request("copyMessage", Some(body)).await
    }

    /// Use this method to send photos. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_photo(&self, body: SendPhoto) -> Result<Message, Error> {
        self.send_request("sendPhoto", Some(body)).await
    }

    /// Use this method to send audio files, if you want Telegram clients to display them in the music player. Your audio must be in the .MP3 or .M4A format. On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send audio files of up to 50 MB in size, this limit may be changed in the future.</p><p>For sending voice messages, use the [`send_invoice`][Self::send_invoice] method instead.
    pub async fn send_audio(&self, body: SendAudio) -> Result<Message, Error> {
        self.send_request("sendAudio", Some(body)).await
    }

    /// Use this method to send general files. On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send files of any type of up to 50 MB in size, this limit may be changed in the future.
    pub async fn send_document(&self, body: SendDocument) -> Result<Message, Error> {
        self.send_request("sendDocument", Some(body)).await
    }

    /// Use this method to send video files, Telegram clients support MPEG4 videos (other formats may be sent as [`Document`][crate::models::Document]). On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
    pub async fn send_video(&self, body: SendVideo) -> Result<Message, Error> {
        self.send_request("sendVideo", Some(body)).await
    }

    /// Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound). On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
    pub async fn send_animation(&self, body: SendAnimation) -> Result<Message, Error> {
        self.send_request("sendAnimation", Some(body)).await
    }

    /// Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message. For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as [`Audio`][crate::models::Audio] or [`Document`][crate::models::Document]). On success, the sent [`Message`][crate::models::Message] is returned. Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future.
    pub async fn send_voice(&self, body: SendVoice) -> Result<Message, Error> {
        self.send_request("sendVoice", Some(body)).await
    }

    /// As of [v.4.0](https://telegram.org/blog/video-messages-and-telescope), Telegram clients support rounded square MPEG4 videos of up to 1 minute long. Use this method to send video messages. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_video_note(&self, body: SendVideoNote) -> Result<Message, Error> {
        self.send_request("sendVideoNote", Some(body)).await
    }

    /// Use this method to send a group of photos, videos, documents or audios as an album. Documents and audio files can be only grouped in an album with messages of the same type. On success, an array of [`Message`][crate::models::Message] that were sent is returned.
    pub async fn send_media_group(&self, body: SendMediaGroup) -> Result<Vec<Message>, Error> {
        self.send_request("sendMediaGroup", Some(body)).await
    }

    /// Use this method to send point on the map. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_location(&self, body: SendLocation) -> Result<Message, Error> {
        self.send_request("sendLocation", Some(body)).await
    }

    /// Use this method to edit live location messages. A location can be edited until its <em>live_period</em> expires or editing is explicitly disabled by a call to [`stop_message_live_location`][Self::stop_message_live_location]. On success, if the edited message is not an inline message, the edited [`Message`][crate::models::Message] is returned, otherwise `true` is returned.
    pub async fn edit_message_live_location(&self, body: EditMessageLiveLocation) -> Result<Message, Error> {
        self.send_request("editMessageLiveLocation", Some(body)).await
    }

    /// Use this method to stop updating a live location message before <em>live_period</em> expires. On success, if the message is not an inline message, the edited [`Message`][crate::models::Message] is returned, otherwise `true` is returned.
    pub async fn stop_message_live_location(&self, body: StopMessageLiveLocation) -> Result<Message, Error> {
        self.send_request("stopMessageLiveLocation", Some(body)).await
    }

    /// Use this method to send information about a venue. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_venue(&self, body: SendVenue) -> Result<Message, Error> {
        self.send_request("sendVenue", Some(body)).await
    }

    /// Use this method to send phone contacts. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_contact(&self, body: SendContact) -> Result<Message, Error> {
        self.send_request("sendContact", Some(body)).await
    }

    /// Use this method to send a native poll. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_poll(&self, body: SendPoll) -> Result<Message, Error> {
        self.send_request("sendPoll", Some(body)).await
    }

    /// Use this method to send an animated emoji that will display a random value. On success, the sent [`Message`][crate::models::Message] is returned.
    pub async fn send_dice(&self, body: SendDice) -> Result<Message, Error> {
        self.send_request("sendDice", Some(body)).await
    }

    /// Use this method when you need to tell the user that something is happening on the bot&#39;s side. The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status). Returns `true` on success.</p><blockquote><p>Example: The [ImageBot](https://t.me/imagebot) needs some time to process a request and upload the image. Instead of sending a text message along the lines of “Retrieving image, please wait…”, the bot may use [`send_chat_action`][Self::send_chat_action] with <em>action</em> = <em>upload_photo</em>. The user will see a “sending photo” status for the bot.</p></blockquote><p>We only recommend using this method when a response from the bot will take a <strong>noticeable</strong> amount of time to arrive.
    pub async fn send_chat_action(&self, body: SendChatAction) -> Result<Message, Error> {
        self.send_request("sendChatAction", Some(body)).await
    }

    /// Use this method to get a list of profile pictures for a user. Returns a[`UserProfilePhotos`][crate::models::UserProfilePhotos] object.
    pub async fn get_user_profile_photos(&self, body: GetUserProfilePhotos) -> Result<UserProfilePhotos, Error> {
        self.send_request("getUserProfilePhotos", Some(body)).await
    }

    /// Use this method to get basic information about a file and prepare it for downloading. For the moment, bots can download files of up to 20MB in size. On success, a [`File`][crate::models::File] object is returned. The file can then be downloaded via the link `https://api.telegram.org/file/bot<token>/<file_path>`, where `<file_path>;` is taken from the response. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling [`get_file`][Self::get_file] again.
    pub async fn get_file(&self, body: GetFile) -> Result<File, Error> {
        self.send_request("getFile", Some(body)).await
    }

    /// Use this method to ban a user in a group, a supergroup or a channel. In the case of supergroups and channels, the user will not be able to return to the chat on their own using invite links, etc., unless [`unbanned`][Self::unban_chat_member] first. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn ban_chat_member(&self, body: BanChatMember) -> Result<bool, Error> {
        self.send_request("banChatMember", Some(body)).await
    }

    /// Use this method to unban a previously banned user in a supergroup or channel. The user will <strong>not</strong> return to the group or channel automatically, but will be able to join via link, etc. The bot must be an administrator for this to work. By default, this method guarantees that after the call the user is not a member of the chat, but will be able to join it. So if the user is a member of the chat they will also be <strong>removed</strong> from the chat. If you don&#39;t want this, use the parameter <em>only_if_banned</em>. Returns `true` on success.
    pub async fn unban_chat_member(&self, body: UnbanChatMember) -> Result<bool, Error> {
        self.send_request("unbanChatMember", Some(body)).await
    }

    /// Use this method to restrict a user in a supergroup. The bot must be an administrator in the supergroup for this to work and must have the appropriate administrator rights. Pass `true` for all permissions to lift restrictions from a user. Returns `true` on success.
    pub async fn restrict_chat_member(&self, body: RestrictChatMember) -> Result<bool, Error> {
        self.send_request("restrictChatMember", Some(body)).await
    }

    /// Use this method to promote or demote a user in a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Pass <em>False</em> for all boolean parameters to demote a user. Returns `true` on success.
    pub async fn promote_chat_member(&self, body: PromoteChatMember) -> Result<bool, Error> {
        self.send_request("promoteChatMember", Some(body)).await
    }

    /// Use this method to set a custom title for an administrator in a supergroup promoted by the bot. Returns `true` on success.
    pub async fn set_chat_administrator_custom_title(&self, body: SetChatAdministratorCustomTitle) -> Result<bool, Error> {
        self.send_request("setChatAdministratorCustomTitle", Some(body)).await
    }

    /// Use this method to ban a channel chat in a supergroup or a channel. Until the chat is [`unbanned`][Self::unban_chat_sender_chat], the owner of the banned chat won&#39;t be able to send messages on behalf of <strong>any of their channels</strong>. The bot must be an administrator in the supergroup or channel for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn ban_chat_sender_chat(&self, body: BanChatSenderChat) -> Result<bool, Error> {
        self.send_request("banChatSenderChat", Some(body)).await
    }

    /// Use this method to unban a previously banned channel chat in a supergroup or channel. The bot must be an administrator for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn unban_chat_sender_chat(&self, body: UnbanChatSenderChat) -> Result<bool, Error> {
        self.send_request("unbanChatSenderChat", Some(body)).await
    }

    /// Use this method to set default chat permissions for all members. The bot must be an administrator in the group or a supergroup for this to work and must have the <em>can_restrict_members</em> administrator rights. Returns `true` on success.
    pub async fn set_chat_permissions(&self, body: SetChatPermissions) -> Result<bool, Error> {
        self.send_request("setChatPermissions", Some(body)).await
    }

    /// Use this method to generate a new primary invite link for a chat; any previously generated primary link is revoked. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns the new invite link as <em>String</em> on success.
    pub async fn export_chat_invite_link(&self, body: ExportChatInviteLink) -> Result<String, Error> {
        self.send_request("exportChatInviteLink", Some(body)).await
    }

    /// Use this method to create an additional invite link for a chat. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. The link can be revoked using the method [`revoke_chat_invite_link`][Self::revoke_chat_invite_link]. Returns the new invite link as [`ChatInviteLink`][crate::models::ChatInviteLink] object.
    pub async fn create_chat_invite_link(&self, body: CreateChatInviteLink) -> Result<ChatInviteLink, Error> {
        self.send_request("createChatInviteLink", Some(body)).await
    }

    /// Use this method to edit a non-primary invite link created by the bot. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns the edited invite link as a [`ChatInviteLink`][crate::models::ChatInviteLink] object.
    pub async fn edit_chat_invite_link(&self, body: EditChatInviteLink) -> Result<ChatInviteLink, Error> {
        self.send_request("editChatInviteLink", Some(body)).await
    }

    /// Use this method to revoke an invite link created by the bot. If the primary link is revoked, a new link is automatically generated. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns the revoked invite link as [`ChatInviteLink`][crate::models::ChatInviteLink] object.
    pub async fn revoke_chat_invite_link(&self, body: RevokeChatInviteLink) -> Result<ChatInviteLink, Error> {
        self.send_request("revokeChatInviteLink", Some(body)).await
    }

    /// Use this method to approve a chat join request. The bot must be an administrator in the chat for this to work and must have the <em>can_invite_users</em> administrator right. Returns `true` on success.
    pub async fn approve_chat_join_request(&self, body: ApproveChatJoinRequest) -> Result<bool, Error> {
        self.send_request("approveChatJoinRequest", Some(body)).await
    }

    /// Use this method to decline a chat join request. The bot must be an administrator in the chat for this to work and must have the <em>can_invite_users</em> administrator right. Returns `true` on success.
    pub async fn decline_chat_join_request(&self, body: DeclineChatJoinRequest) -> Result<bool, Error> {
        self.send_request("declineChatJoinRequest", Some(body)).await
    }

    /// Use this method to set a new profile photo for the chat. Photos can&#39;t be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn set_chat_photo(&self, body: SetChatPhoto) -> Result<bool, Error> {
        self.send_request("setChatPhoto", Some(body)).await
    }

    /// Use this method to delete a chat photo. Photos can&#39;t be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn delete_chat_photo(&self, body: DeleteChatPhoto) -> Result<bool, Error> {
        self.send_request("deleteChatPhoto", Some(body)).await
    }

    /// Use this method to change the title of a chat. Titles can&#39;t be changed for private chats. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn set_chat_title(&self, body: SetChatTitle) -> Result<bool, Error> {
        self.send_request("setChatTitle", Some(body)).await
    }

    /// Use this method to change the description of a group, a supergroup or a channel. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Returns `true` on success.
    pub async fn set_chat_description(&self, body: SetChatDescription) -> Result<bool, Error> {
        self.send_request("setChatDescription", Some(body)).await
    }

    /// Use this method to add a message to the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the &#39;can_pin_messages&#39; administrator right in a supergroup or &#39;can_edit_messages&#39; administrator right in a channel. Returns `true` on success.
    pub async fn pin_chat_message(&self, body: PinChatMessage) -> Result<bool, Error> {
        self.send_request("pinChatMessage", Some(body)).await
    }

    /// Use this method to remove a message from the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the &#39;can_pin_messages&#39; administrator right in a supergroup or &#39;can_edit_messages&#39; administrator right in a channel. Returns `true` on success.
    pub async fn unpin_chat_message(&self, body: UnpinChatMessage) -> Result<bool, Error> {
        self.send_request("unpinChatMessage", Some(body)).await
    }

    /// Use this method to clear the list of pinned messages in a chat. If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the &#39;can_pin_messages&#39; administrator right in a supergroup or &#39;can_edit_messages&#39; administrator right in a channel. Returns `true` on success.
    pub async fn unpin_all_chat_messages(&self, body: UnpinAllChatMessages) -> Result<bool, Error> {
        self.send_request("unpinAllChatMessages", Some(body)).await
    }

    /// Use this method for your bot to leave a group, supergroup or channel. Returns `true` on success.
    pub async fn leave_chat(&self, body: LeaveChat) -> Result<bool, Error> {
        self.send_request("leaveChat", Some(body)).await
    }

    /// Use this method to get up to date information about the chat (current name of the user for one-on-one conversations, current username of a user, group or channel, etc.). Returns a [`Chat`][crate::models::Chat] object on success.
    pub async fn get_chat(&self, body: GetChat) -> Result<Chat, Error> {
        self.send_request("getChat", Some(body)).await
    }

    /// Use this method to get a list of administrators in a chat, which aren&#39;t bots. Returns an Array of [`ChatMember`][crate::models::ChatMember] objects.
    pub async fn get_chat_administrators(&self, body: GetChatAdministrators) -> Result<Vec<ChatMember>, Error> {
        self.send_request("getChatAdministrators", Some(body)).await
    }

    /// Use this method to get the number of members in a chat. Returns <em>Int</em> on success.
    pub async fn get_chat_member_count(&self, body: GetChatMemberCount) -> Result<i64, Error> {
        self.send_request("getChatMemberCount", Some(body)).await
    }

    /// Use this method to get information about a member of a chat. Returns a [`ChatMember`][crate::models::ChatMember] object on success.
    pub async fn get_chat_member(&self, body: GetChatMember) -> Result<ChatMember, Error> {
        self.send_request("getChatMember", Some(body)).await
    }

    /// Use this method to set a new group sticker set for a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Use the field <em>can_set_sticker_set</em> optionally returned in [`get_chat`][Self::get_chat] requests to check if the bot can use this method. Returns `true` on success.
    pub async fn set_chat_sticker_set(&self, body: SetChatStickerSet) -> Result<bool, Error> {
        self.send_request("setChatStickerSet", Some(body)).await
    }

    /// Use this method to delete a group sticker set from a supergroup. The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights. Use the field <em>can_set_sticker_set</em> optionally returned in [`get_chat`][Self::get_chat] requests to check if the bot can use this method. Returns `true` on success.
    pub async fn delete_chat_sticker_set(&self, body: DeleteChatStickerSet) -> Result<bool, Error> {
        self.send_request("deleteChatStickerSet", Some(body)).await
    }

    /// Use this method to send answers to callback queries sent from [`inline keyboards`](https://core.telegram.org/bots/features#inline-keyboards). The answer will be displayed to the user as a notification at the top of the chat screen or as an alert. On success, `true` is returned.</p><blockquote><p>Alternatively, the user can be redirected to the specified Game URL. For this option to work, you must first create a game for your bot via [@BotFather](https://t.me/botfather) and accept the terms. Otherwise, you may use links like <code>t.me/your_bot?start=XXXX</code> that open your bot with a parameter
    pub async fn answer_callback_query(&self, body: AnswerCallbackQuery) -> Result<bool, Error> {
        self.send_request("answerCallbackQuery", Some(body)).await
    }

    /// Use this method to change the list of the bot's commands. See this manual for more details about bot commands. Returns `true` on success
    pub async fn set_my_commands(&self, body: SetMyCommands) -> Result<bool, Error> {
        self.send_request("setMyCommands", Some(body)).await
    }

    /// Use this method to delete the list of the bot&#39;s commands for the given scope and user language. After deletion, [higher level commands](https://core.telegram.org/bots/api#determining-list-of-commands) will be shown to affected users. Returns `true` on success.
    pub async fn delete_my_commands(&self, body: DeleteMyCommands) -> Result<bool, Error> {
        self.send_request("deleteMyCommands", Some(body)).await
    }

    /// Use this method to get the current list of the bot&#39;s commands for the given scope and user language. Returns an Array of [`BotCommand`][crate::models::BotCommand] objects. If commands aren&#39;t set, an empty list is returned.
    pub async fn get_my_commands(&self, body: GetMyCommands) -> Result<Vec<BotCommand>, Error> {
        self.send_request("getMyCommands", Some(body)).await
    }

    /// Use this method to change the bot&#39;s menu button in a private chat, or the default menu button. Returns `true` on success.
    pub async fn set_chat_menu_button(&self, body: SetChatMenuButton) -> Result<bool, Error> {
        self.send_request("setChatMenuButton", Some(body)).await
    }

    /// Use this method to get the current value of the bot&#39;s menu button in a private chat, or the default menu button. Returns [`MenuButton`][crate::models::MenuButton] on success.
    pub async fn get_chat_menu_button(&self, body: GetChatMenuButton) -> Result<MenuButton, Error> {
        self.send_request("getChatMenuButton", Some(body)).await
    }

    /// Use this method to change the default administrator rights requested by the bot when it&#39;s added as an administrator to groups or channels. These rights will be suggested to users, but they are are free to modify the list before adding the bot. Returns `true` on success.
    pub async fn set_my_default_administrator_rights(&self, body: SetMyDefaultAdministratorRights) -> Result<bool, Error> {
        self.send_request("setMyDefaultAdministratorRights", Some(body)).await
    }

    /// Use this method to get the current default administrator rights of the bot. Returns [`ChatAdministratorRights`][crate::models::ChatAdministratorRights] on success.
    pub async fn get_my_default_administrator_rights(&self, body: GetMyDefaultAdministratorRights) -> Result<ChatAdministratorRights, Error> {
        self.send_request("getMyDefaultAdministratorRights", Some(body)).await
    }

    /// Use this method to send invoices. On success, the sent Message is returned.
    pub async fn send_invoice(&self, body: SendInvoice) -> Result<Message, Error> {
        self.send_request("sendInvoice", Some(body)).await
    }

    /// Use this method to create a link for an invoice. Returns the created invoice link as String on success.
    pub async fn create_invoice_link(&self, body: CreateInvoiceLink) -> Result<String, Error> {
        self.send_request("createInvoiceLink", Some(body)).await
    }

    /// If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will send an Update with a shipping_query field to the bot. Use this method to reply to shipping queries. On success, `true` is returned.
    pub async fn answer_shipping_query(&self, body: AnswerShippingQuery) -> Result<bool, Error> {
        self.send_request("answerShippingQuery", Some(body)).await
    }

    /// Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the form of an Update with the field pre_checkout_query. Use this method to respond to such pre-checkout queries. On success, `true` is returned. Note: The Bot API must receive an answer within 10 seconds after the pre-checkout query was sent.
    pub async fn answer_pre_checkout_query(&self, body: AnswerPreCheckoutQuery) -> Result<bool, Error> {
        self.send_request("answerPreCheckoutQuery", Some(body)).await
    }

    /// Informs a user that some of the Telegram Passport elements they provided contains errors. The user will not be able to re-submit their Passport to you until the errors are fixed (the contents of the field for which you returned the error must change). Returns `true` on success.
    ///
    /// Use this if the data submitted by the user doesn't satisfy the standards your service requires for any reason. For example, if a birthday date seems invalid, a submitted document is blurry, a scan shows evidence of tampering, etc. Supply some details in the error message to make sure the user knows how to correct the issues.
    pub async fn set_password_data_errors(&self, body: SetPasswordDataErrors) -> Result<bool, Error> {
        self.send_request("setPasswordDataErrors", Some(body)).await
    }

    /// Use this method to send a game. On success, the sent Message is returned.
    pub async fn send_game(&self, body: SendGame) -> Result<Message, Error> {
        self.send_request("sendGame", Some(body)).await
    }

    /// Use this method to set the score of the specified user in a game message. On success, if the message is not an inline message, the Message is returned, otherwise `true` is returned. Returns an error, if the new score is not greater than the user's current score in the chat and force is False.
    pub async fn set_game_score(&self, body: SetGameScore) -> Result<bool, Error> {
        self.send_request("setGameScore", Some(body)).await
    }

    /// Use this method to get data for high score tables. Will return the score of the specified user and several of their neighbors in a game. Returns an Array of GameHighScore objects.
    ///
    /// This method will currently return scores for the target user, plus two of their closest neighbors on each side. Will also return the top three users if the user and their neighbors are not among them. Please note that this behavior is subject to change.
    pub async fn get_game_high_scores(&self, body: GetGameHighScores) -> Result<Vec<GameHighScore>, Error> {
        self.send_request("getGameHighScores", Some(body)).await
    }

    /// Use this method to send answers to an inline query. On success, `true` is returned.
    pub async fn answer_inline_query(&self, body: AnswerInlineQuery) -> Result<Vec<bool>, Error> {
        self.send_request("answerInlineQuery", Some(body)).await
    }

    /// Use this method to set the result of an interaction with a Web App and send a corresponding message on behalf of the user to the chat from which the query originated. On success, a SentWebAppMessage object is returned.
    pub async fn answer_webapp_query(&self, body: AnswerWebAppQuery) -> Result<Vec<SentWebAppMessage>, Error> {
        self.send_request("answerWebAppQuery", Some(body)).await
    }

    /// Use this method to create a topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. Returns information about the created topic as a ForumTopic object.
    pub async fn create_forum_topic(&self, body: CreateForumTopic) -> Result<Message, Error> {
        self.send_request("createForumTopic", Some(body)).await
    }

    /// Use this method to edit name and icon of a topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have can_manage_topics administrator rights, unless it is the creator of the topic. Returns True on success.
    pub async fn edit_forum_topic(&self, body: EditForumTopic) -> Result<Message, Error> {
        self.send_request("editForumTopic", Some(body)).await
    }

    /// Use this method to close an open topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights, unless it is the creator of the topic. Returns True on success.
    pub async fn close_forum_topic(&self, body: CloseForumTopic) -> Result<Message, Error> {
        self.send_request("closeForumTopic", Some(body)).await
    }

    /// Use this method to reopen a closed topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights, unless it is the creator of the topic. Returns True on success.
    pub async fn reopen_forum_topic(&self, body: ReopenForumTopic) -> Result<Message, Error> {
        self.send_request("reopenForumTopic", Some(body)).await
    }

    /// Use this method to delete a forum topic along with all its messages in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_delete_messages administrator rights. Returns True on success.
    pub async fn delete_forum_topic(&self, body: DeleteForumTopic) -> Result<Message, Error> {
        self.send_request("deleteForumTopic", Some(body)).await
    }

    /// Use this method to clear the list of pinned messages in a forum topic. The bot must be an administrator in the chat for this to work and must have the can_pin_messages administrator right in the supergroup. Returns True on success.
    pub async fn unpin_all_forum_topic_message(&self, body: UnpinAllForumTopicMessages) -> Result<Message, Error> {
        self.send_request("unpinAllForumTopicMessages", Some(body)).await
    }

    /// Use this method to edit the name of the 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have can_manage_topics administrator rights. Returns True on success.
    pub async fn edit_general_forum_topic(&self, body: EditGeneralForumTopic) -> Result<bool, Error> {
        self.send_request("editGeneralForumTopic", Some(body)).await
    }

    /// Use this method to close an open 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. Returns True on success.
    pub async fn close_general_forum_topic(&self, body: CloseGeneralForumTopic) -> Result<bool, Error> {
        self.send_request("closeGeneralForumTopic", Some(body)).await
    }

    /// Use this method to reopen a closed 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. The topic will be automatically unhidden if it was hidden. Returns True on success.
    pub async fn reopen_general_forum_topic(&self, body: ReopenGeneralForumTopic) -> Result<bool, Error> {
        self.send_request("reopenGeneralForumTopic", Some(body)).await
    }

    /// Use this method to hide the 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. The topic will be automatically closed if it was open. Returns True on success.
    pub async fn hide_general_forum_topic(&self, body: HideGeneralForumTopic) -> Result<bool, Error> {
        self.send_request("hideGeneralForumTopic", Some(body)).await
    }

    /// Use this method to unhide the 'General' topic in a forum supergroup chat. The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights. Returns True on success.
    pub async fn unhide_general_forum_topic(&self, body: UnhideGeneralForumTopic) -> Result<bool, Error> {
        self.send_request("unhideGeneralForumTopic", Some(body)).await
    }
}
