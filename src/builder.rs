/// Definer a builder for a given structure
///
/// The builder should be derived with [`#[derive(Builder)]`](macro@crate::Builder)\
/// It will have the following properties:
/// - A new() function, with parameters for each non-optional field of the structure
/// - A method for each optional parameter to initialize that field. It returns self to allow chained calls
pub trait Builder {
    /// Type to be built
    type Value;

    /// Converts the builder into the built value
    fn build(self) -> Self::Value;
}
