use log::error;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};

/// Any error that may occur when sending a request to Telegram's API
#[derive(Debug)]
pub enum Error {
    /// Error while parsing the query result. This error should never occur, if it does, please open a issue on the [repo]("https://www.gitlab.com/Thechi2000/telegram-api-rut/issues)
    ParseError(serde_json::Error),
    /// Error while reading files to upload
    IOError(std::io::Error),
    /// Error while communicating with the Telegram server
    RequestError(reqwest::Error),
    /// Error returned by the API
    TelegramError(TelegramError),
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum Response<A> {
    Ok { ok: bool, result: A },

    Error { ok: bool, error_code: u32, description: String },
}

/// Describe an error returned by the API
#[derive(Serialize, Deserialize, Debug)]
pub struct TelegramError {
    /// Whether the request was successful (always false) TODO remove
    ok: bool,
    error_code: u32,
    description: String,
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::ParseError(e)
    }
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Error::RequestError(e)
    }
}

impl From<TelegramError> for Error {
    fn from(e: TelegramError) -> Self {
        Error::TelegramError(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IOError(e)
    }
}

pub(crate) fn parse_api_result<A: DeserializeOwned>(text: String) -> Result<A, Error> {
    #[derive(Deserialize)]
    struct O<B> {
        ok: bool,
        result: B,
    }

    match serde_json::from_str::<Response<A>>(text.as_str()).map_err(|e| {
        error!("Could not parse telegram response. Please report this issue on https://www.github.com/Thechi2000/telegram-api-rust\nServer response: {}", text);
        Error::ParseError(e)
    })? {
        Response::Ok { ok: _, result } => Ok(result),
        Response::Error { ok, error_code, description } => Err(Error::TelegramError(TelegramError { ok, error_code, description })),
    }
}
