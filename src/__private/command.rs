use crate::__private::HandlerError;
use crate::bot::Bot;
use crate::models::Update;
use crate::state::BotState;
use async_trait::async_trait;
use std::collections::btree_map::BTreeMap;

/// Stores a command Handler implementation and some additional infos\
/// This structure should not be used, instead use the [`command`](crate::command) macro
pub struct CommandHandler<T> {
    /// Name of the command (the word following the '/⁾)
    pub name: String,
    pub syntax: &'static str,
    /// Priority of this handler for the command
    pub rank: u8,
    pub handler: Box<dyn Handler<T>>,
}

/// Groups a list of commands into a map where the key is the name of the command\
/// This function should not be used, instead use the [`commands`](crate::commands) macro
pub fn group_commands<T>(commands: Vec<CommandHandler<T>>) -> BTreeMap<String, Vec<CommandHandler<T>>> {
    commands.into_iter().fold(BTreeMap::new(), |mut map, cmd| {
        if let Some(vec) = map.get_mut(&cmd.name) {
            vec.push(cmd);
            vec.sort_by_key(|c| c.rank);
        } else {
            map.insert(cmd.name.clone(), vec![cmd]);
        }

        map
    })
}

/// Handles an incoming command\
/// This trait should not be manually implemented, instead use the [`command`](crate::command) macro
#[async_trait]
pub trait Handler<T: Sync> {
    /// Handles an incoming command
    async fn handle(&self, _: &[String], _: &Update, _: &Bot, _: Option<&BotState<T>>) -> Result<(), HandlerError>;
}
