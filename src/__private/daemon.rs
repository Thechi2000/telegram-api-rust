use crate::{Bot, BotState};
use std::sync::Arc;
use tokio::sync::Mutex;

pub struct DaemonStruct<T>
where
    T: Sync + Send,
{
    pub daemon: Box<dyn Daemon<T> + Sync + Send>,
    pub syntax: &'static str,
}

#[async_trait::async_trait]
pub trait Daemon<T>
where
    T: Sync + Send,
{
    async fn launch(&self, bot: Arc<Bot>, state: Arc<Option<BotState<T>>>, lock: Arc<Mutex<()>>);
}
