use async_trait::async_trait;

#[cfg(feature = "daemons")]
pub mod daemon;

#[cfg(feature = "commands")]
pub mod command;

use crate::bot::Bot;
use crate::models::{Update, UpdateType};
use crate::state::BotState;

pub struct GenericHandler<T>
where
    T: Sync,
{
    pub rank: usize,
    pub name: &'static str,
    pub handler: Box<dyn Handler<T>>,
    pub updates: Vec<UpdateType>,
}

/// Handles an incoming update\
/// This trait should not be manually implemented, instead use the [`handler`](crate::handler) and [`command`](crate::command) macros
#[async_trait]
pub trait Handler<T: Sync> {
    /// Handles an incoming update
    async fn handle(&self, bot: &Bot, update: &Update, state: Option<&BotState<T>>) -> Result<(), HandlerError>;
}

/// Error returned by a handler\
/// This enum should not be used
#[derive(Debug)]
pub enum HandlerError {
    /// Could not parse arguments, delegates to the next handler
    Parse,
    /// Parsing successful, but the handler failed, stop the update processing
    Runtime,
}
