use std::ops::Deref;

/// Contains the state of the bot. Currently only contains a user defined object, but will be augmented with other fields
pub struct BotState<T>
where
    T: Sync,
{
    /// User defined state
    pub state: T,
}

impl<T: Sync> Deref for BotState<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.state
    }
}
