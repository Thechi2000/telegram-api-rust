use crate::{Bot, BotState};

/// Allow a parameter to be parsed for a daemon call.
/// Any type implementing this trait can be used as parameter in a daemon
pub trait FromDaemon<'r, T: Sync>
where
    Self: Sized,
{
    /// Instantiate `Self` from a daemon
    fn from_daemon(bot: &'r Bot, state: Option<&'r BotState<T>>) -> Self;
}

impl<'r, T: Sync> FromDaemon<'r, T> for &'r Bot {
    fn from_daemon(bot: &'r Bot, _: Option<&'r BotState<T>>) -> Self {
        bot
    }
}

impl<'r, T: Sync> FromDaemon<'r, T> for &'r BotState<T> {
    fn from_daemon(_: &'r Bot, state: Option<&'r BotState<T>>) -> Self {
        state.expect("Missing state")
    }
}
